//
//  ChinViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface ChinViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *smallButton;
@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *wideButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *chinImageButton;
@property (retain, nonatomic) IBOutlet UIButton *smallImageButton;
@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *wideImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;


- (IBAction)smallButtonDidTap:(id)sender;
- (IBAction)averageButtonDidTap:(id)sender;
- (IBAction)wideButtonDidTap:(id)sender;

- (IBAction)backButtonDidTap:(id)sender;


@end
