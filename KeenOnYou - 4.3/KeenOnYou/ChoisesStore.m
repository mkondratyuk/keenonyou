//
//  ChoisesStore.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChoisesStore.h"
#import "Choise.h"

static ChoisesStore *defaultStore = nil;



@implementation ChoisesStore

@synthesize allChoices;

+ (ChoisesStore *) defaultStore
{
    if( !defaultStore)
    {
        defaultStore = [[ChoisesStore alloc] init];
    }
    return defaultStore;
}

- (id) init 
{
    if (defaultStore)
    {
        return defaultStore;
    }
    
    self = [super init];
    if (self)
    {
//  Top Of Head
        // Level Heard for Top Of Head  
       NSDictionary *levelHead = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",  nil];
       //  High Front  for Top Of Head
        NSString *decsHF = @"<b>A Thinker</b>,They love to think and can either run out of time to do things or believe it has been done due to having thought through it so much. Also, can be distracted by looking into alternative avenues to complete a task and while actually physically completing a task can seem like a interruption.";
        NSString *ifYouHF = @"<b>Focus</b>,Try to focus on what you are doing and complete one thing a day from a list.";
        NSDictionary *highFront = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:decsHF], @"Desc", ifYouHF, @"IfYou",   nil];
        // High Back   for Top Of Head
        NSString *decsHB = @"<b>Do then Think</b>,Take action first then think about it later, as if it needs doing it needs to be done now.";
        NSString *ifYouHB = @"<b>Stop and think</b>,Stop and think about your actions before doing it.";
        NSDictionary *highBack = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:decsHB], @"Desc", ifYouHB, @"IfYou",   nil];
        
        NSDictionary * topOfHeard = [NSDictionary dictionaryWithObjectsAndKeys:
            highFront, @"HighFront", highBack, @"HighBack",   levelHead,@"LevelHead", nil ];
//  Face Shape
        //  Long and Thin  for Face Shape
        NSString *decsLF_1 = @"<b>Repetitive Learner</b>,Learn step by step and through repetition. As they work hard to learn what they know, they are proud of their accomplishments.";
        NSString *decsLF_2 = @"<b>Resistant</b>,If pushed to work harder they will do so, yet they will fight every step of the way.";
        NSDictionary *longAndThin = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObjects:decsLF_1, decsLF_2, nil], @"Desc", @"", @"IfYou",   nil];
        // Average  for Face Shape
        NSDictionary *averageFace = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art", [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];        
        // Wide face  for Face Shape
        NSString *decsWF_1 = @"<b>Self-confident</b>,Very self-confident and doesn’t understand why everyone don’t have the same amount of confidence as they do.";
        NSString *decsWF_2 = @"<b>Go Getting</b>,Fly through obstacles and sometimes do not even know that they have.";
        NSString *decsWF_3 = @"<b>Needy</b>,Want to be needed and involved, if not they become discouraged. Also, as they need to be wanted, they will take on more than you can handle.";        
        NSString *ifYouWB = @"<b>Support</b>,Support people, instead of doing it for them, just because you can.";
        NSDictionary *wideFace = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObjects:decsWF_1,decsWF_2,decsWF_3, nil], @"Desc", ifYouWB, @"IfYou",   nil];
        
        NSDictionary * faceShape = [NSDictionary dictionaryWithObjectsAndKeys:
            longAndThin, @"LongAndThin", averageFace, @"Average", wideFace,@"Wide", nil ];
//   Lips
        //  Thin top and bottom  for Lips
        NSString *decsThin_1 = @"<b>Responsible, hard working</b>,Responsible, hard working and a little more serious than others. May be harsh in judging themselves and in turn expect high standards of others.";
        NSString *decsThin_2 = @"<b>Feelings</b>,It is hard for them to say how they feel, so ask them what they are thinking. If they do say how they feel, make sure you  listen as it may not happen often.";
        NSDictionary *thinTopAndBottom = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObjects:decsThin_1, decsThin_2, nil], @"Desc", @"", @"IfYou",   nil];
        // Thin top Thick bottom  for Lips
        NSString *decsThinThick_1 = @"<b>Feelings</b>,It is hard for them to say how they feel, so ask them what they are thinking. If they do say how they feel, make sure you  listen as it may not happen often.";
        NSString *decsThinThick_2 = @"<b>Giving</b>,They are generous and the bigger their bottom lip the more generous they are.";
        NSDictionary *thinTopAndThickBottom = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Lips_ThinTop-ThickBottom.png", @"Art",  [NSArray arrayWithObjects:decsThinThick_1, decsThinThick_2, nil], @"Desc", @"", @"IfYou",   nil];
        // Thick top Thin bottom  for Lips
        NSString *decsThickThin_1 = @"<b>Talker</b>,The thicker the top lip the more they want to talk.";
        NSString *decsThickThin_2 = @"<b>Receive</b>,Good at receiving love and expressing their feelings.";
        NSDictionary *thickTopAndThinBottom = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Lips_ThickTopAndThinBottom.png", @"Art",  [NSArray arrayWithObjects:decsThickThin_1, decsThickThin_2, nil], @"Desc", @"", @"IfYou",   nil];
        // Thick top and bottom  for Lips 
        NSString *decsThick_1 = @"<b>Fun loving</b>,Enjoys the pleasures of life, plan less and the larger their lips the better the ability to relax.";
        NSString *decsThick_2 = @"<b>Talker</b>,The thicker the top lip the more they want to talk.";
        NSString *decsThick_3 = @"<b>Giving</b>,They are generous and the bigger their bottom lip the more generous they are.";
        NSString *decsThick_4 = @"<b>Receive</b>,Good at receiving love and expressing their feelings";        
        NSDictionary *thickTopAndBottom = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Lips_ThickTopAndBottom.png", @"Art",  [NSArray arrayWithObjects:decsThick_1, decsThick_2, decsThick_3, decsThick_4 ,nil], @"Desc", @"", @"IfYou",   nil];
        
        NSDictionary * lips = [NSDictionary dictionaryWithObjectsAndKeys:
            thinTopAndBottom, @"ThinAndThin", thinTopAndThickBottom, @"ThinAndThick", thickTopAndThinBottom,@"ThickAndThin", thickTopAndBottom, @"ThickAndThick", nil ];
//   Eyebrow Shapes
        //  Average  for Eyebrow Shapes
        NSDictionary *averageEyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Eyebrows_Average.png", @"Art", [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];
        //  Thick  for Eyebrow Shapes
        NSString *descThick = @"<b>Continual Thinker</b>,Only if the eyebrows connect, then they continual think. They find it hard to relax and are always thinking about an issue, situation or something all the time";
        NSString *ifYouThick = @"<b>Learn how to relax</b>,Try meditation or another form of relaxation technique.";        
        NSDictionary *thickEyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Eyebrows_Thick.png", @"Art", [NSArray arrayWithObject:descThick], @"Desc", ifYouThick, @"IfYou",   nil]; 
        //  Straight  for Eyebrow Shapes
        NSString *descSt = @"<b>Reserved and Controlling</b>,Reserved personality, likes to be in control, but do not like confrontation.";
        NSString *ifYouSt = @"<b>High Arc eyebrows</b>,Be aware that those  with High Arc eyebrows can make you uneasy due to their openness and expression of their feelings.";        
        NSDictionary *straightEyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Eyebrows_Straight.png", @"Art", [NSArray arrayWithObject:descSt], @"Desc", ifYouSt, @"IfYou",   nil]; 
        //  Sharp Point  for Eyebrow Shapes
        NSString *descShP_1 = @"<b>Big picture</b>,They can see the big picture from start to finish. They are good in a Producer position and are usually self-employed.";
        NSString *descShP_2 = @"<b>Need to Produce</b>,They have a feeling of wanting to produce their ideas, if they are unable to do so they can become moody or depressed.";        
        NSString *ifYouShP = @"";        
        NSDictionary *sharpPointEyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Eyebrows_SharpPoint.png", @"Art", [NSArray arrayWithObjects:descShP_1, descShP_2, nil], @"Desc", ifYouShP, @"IfYou",   nil]; 
        //  Semi-Circle  for Eyebrow Shapes
        NSString *descSemC = @"<b>Union</b>,They see how things can come together and can be in perfect harmony with others.";
        NSDictionary *semiCircleEyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Eyebrows_Semi-Circle.png", @"Art", [NSArray arrayWithObject:descSemC], @"Desc", @"", @"IfYou",   nil];
        // High Arc for Eyebrow Shapes
        NSString *descHiA = @"<b>Expressive and Dramatic</b>,They will express what they feel in many ways, indulge in their emotions and can be dramatic."; 
        NSString *ifYouHiA = @"<b>Straight Eyebrows</b>,Be aware that those with very straight eyebrows can be confronted by how expressive you are with your feelings.";
        NSDictionary *highArcCircleEyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Eyebrows_HighArc.png", @"Art", [NSArray arrayWithObject:descHiA], @"Desc", ifYouHiA, @"IfYou",   nil]; 
        
        NSDictionary * eyebrowShapes = [NSDictionary dictionaryWithObjectsAndKeys:
            averageEyebrowShapes, @"Average", thickEyebrowShapes, @"Thick", straightEyebrowShapes,@"Straight", semiCircleEyebrowShapes, @"SemiCircle", sharpPointEyebrowShapes, @"SharpPoint", highArcCircleEyebrowShapes, @"HighArc", nil ];
        
//  Eyebrow Height
        // High for Eyebrow Height
        NSString *descHigh_1 = @"<b>Selective and discerning</b>,More selective in what they do. They also have good judgment and understanding of things.";
        NSString *descHigh_2 = @"<b>Untouchable</b>,They may not be a touchy feely person, so reduce casual touching if irritation is evident.";
        NSString *descHigh_3 = @"<b>Formal</b>,Treat them with more formality, especially when in public";
        NSString *ifYouHigh = @"<b>First Move</b>,Try making the first move in a relationship.";        
        NSDictionary *highEyebrowHeight = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObjects:descHigh_1, descHigh_2, descHigh_3, nil], @"Desc", ifYouHigh, @"IfYou",   nil];
        // Average for Eyebrow Height
        NSDictionary *averageEyebrowHeight = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];
        // Low for Eyebrow Height
        NSString *descLow_1 = @"<b>Informal</b>,The lower the eyebrow the less formal they can be.";
        NSString *descLow_2 = @"<b>Touchy Feely</b>,Touchy feely personality and use touch to know someone.";
        NSDictionary *lowEyebrowHeight = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art", [NSArray arrayWithObjects:descLow_1, descLow_2, nil], @"Desc", @"", @"IfYou",   nil]; 
        
        NSDictionary * eyebrowHeight = [NSDictionary dictionaryWithObjectsAndKeys:
            highEyebrowHeight, @"High", averageEyebrowHeight, @"Average", lowEyebrowHeight,@"Low", nil ];
//  Eye Spacing
        //  Wide    for Eye Spacing
        NSString *descW_1 = @"<b>Whole Picture</b>,See the whole picture and feel everything will take care of itself given time.";
        NSString *descW_2 = @"<b>Time Perspective</b>,Where they are, can be more important to where they are going, so they can often be late.";
        NSString *ifYouW = @"<b>Integrity</b>,If you are always late, find a way to keep your integrity by being on time.";        
        NSDictionary *wideEyeSpacing = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObjects:descW_1, descW_2, nil], @"Desc", ifYouW, @"IfYou",   nil];        
        //  Average for Eye Spacing
        NSDictionary *averageEyeSpacing = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];     
        //  Close   for     Eye Spacing 
        NSString *descClose = @"<b>Focused</b>,Very focused on the now, having things done correctly and done right away.";
        NSDictionary *closeEyeSpacing = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:descClose], @"Desc", @"", @"IfYou",   nil]; 
       
        NSDictionary * eyeSpacing = [NSDictionary dictionaryWithObjectsAndKeys:
            wideEyeSpacing, @"Wide", averageEyeSpacing, @"Average", closeEyeSpacing,@"Close", nil ];
//  Cheekbones
        //  High/Defined    for Cheekbones
        NSString *descHD = @"<b>Adventurous and Courageous</b>,The higher and more protruding the cheekbones are, the more adventure seeking and courageous they are.";
        NSString *ifYouHD = @"<b>Reignite the Flame</b>,Keep excitement in the relationship, for example travel , exploring new places and experiences.";        
        NSDictionary *highOrDefinedCheekbones = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Cheekbones_HighDefined.png", @"Art",  [NSArray arrayWithObject:descHD], @"Desc", ifYouHD, @"IfYou",   nil];  
        //  Soft            for Cheekbones
        NSDictionary *softCheekbones = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Cheekbones_Soft.png", @"Art", [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];    
        //  Average         for Cheekbones
        NSDictionary *averageCheeckbones = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];
        
        NSDictionary * cheekbones = [NSDictionary dictionaryWithObjectsAndKeys:
            highOrDefinedCheekbones, @"HighDef", averageCheeckbones, @"Average", softCheekbones,@"Soft", nil ];
        
//  Nostril
        //  Narrow    for Nostril
        NSString *descNN_1 = @"<b>Frugal</b>,When it comes to money they like to keep it close and spend little.";
        NSString *descNN_2 = @"<b>Reliant</b>,Can be very reliant on others.";
        NSString *ifYouNN = @"<b>Decisive</b>,Need to be confident and decisive in your own decisions.";
        NSDictionary *narrowNostrils = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Nostril_Narrow.png", @"Art", [NSArray arrayWithObjects:descNN_1, descNN_2, nil], @"Desc", ifYouNN, @"IfYou",   nil ]; 
        //  Average for Nostrils
        NSDictionary *averageNostrils = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Nostril_Average.png", @"Art", [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];        
        //  Wide  for Nostrils
        NSString *descWN_1 = @"<b>Spender</b>,When it come to money they like to spend.";
        NSString *descWN_2 = @"<b>Self-reliant</b>,The wider the nostrils the more self-reliant the person is.";
        NSDictionary *wideNostrils = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Nostril_Wide.png", @"Art", [NSArray arrayWithObjects:descWN_1, descWN_2, nil], @"Desc", @"", @"IfYou",   nil];
        
        NSDictionary *nostrils = [NSDictionary dictionaryWithObjectsAndKeys:
                narrowNostrils, @"Narrow", averageNostrils, @"Average", wideNostrils,@"Wide", nil ];
// Nose Underside
        //  Angled Up   for Nose Underside  
        NSString *descAU = @"<b>Trusting</b>,They trust most things and are not usually sceptical.";
        NSDictionary *angledUpNoseUnderside = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:descAU], @"Desc", @"", @"IfYou",   nil];
        //  Straight   for Nose Underside
        NSDictionary *straightNoseUnderside = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];
        
        //  Angled Down   for Nose Underside
        NSString *descAD_1 = @"<b>Sceptic</b>,They doubt and mistrust people, ideas and things in general.";
        NSString *descAD_2 = @"<b>Fact Finder</b>,Like to gather all the facts and figures before making a decision or getting into anything.";
        NSDictionary *angledDownNoseUnderside = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObjects:descAD_1, descAD_2, nil], @"Desc", @"", @"IfYou",   nil];
        
        NSDictionary *noseUnderside = [NSDictionary dictionaryWithObjectsAndKeys:
                angledUpNoseUnderside, @"Up", straightNoseUnderside, @"Straight", angledDownNoseUnderside,@"Down", nil ];
//  Chin
        //  Small   for Chin
        NSString *descSC = @"<b>Sensitive</b>,Very sensitive to criticism and overwhelming emotional situations. Be cautious when criticizing them and be more nurturing than usual. They may also be very tender and compliant.";
        NSDictionary *smallChin = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Chin_Small.png", @"Art",  [NSArray arrayWithObject:descSC], @"Desc", @"", @"IfYou",   nil];   
         //  Average   for Chin 
        NSDictionary *averageChin = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];
        
        //  Wide   for Chin 
        NSString *descWC_1 = @"<b>Tough and Demanding</b>,Can take criticism well, may also have high standards and expectations";
        NSString *descWC_2 = @"<b>Tenacious</b>,If chin protrudes more than usual, then they are tenacious and will not let go of things easily , holding onto them as best they can.";     
        NSDictionary *wideChin = [NSDictionary dictionaryWithObjectsAndKeys:
            @"Chin_Wide.png", @"Art",  [NSArray arrayWithObjects:descWC_1, descWC_2, nil], @"Desc", @"", @"IfYou",   nil];
        
        NSDictionary *chin = [NSDictionary dictionaryWithObjectsAndKeys:
            smallChin, @"Small", averageChin, @"Average", wideChin,@"Wide", nil ];

//  Mouth Corners    
        // Turns Up for Mouth Corners
        NSString *descMCTU = @"<b>Optimist</b>,Has positive outlook on life.";
        NSDictionary *turnsUpMouthConers = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:descMCTU], @"Desc", @"", @"IfYou",   nil];
        
        // Turns Down for Mouth Corners
        NSString *descMCTD = @"<b>Pessimist</b>,Expects the worst to happen.";
        NSDictionary *turnsDownMouthConers = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:descMCTD], @"Desc", @"", @"IfYou",   nil];
        // Straight for Mouth Corners
        NSDictionary *straightMouthConers = [NSDictionary dictionaryWithObjectsAndKeys:
            @"", @"Art",  [NSArray arrayWithObject:[NSNull null]], @"Desc", @"", @"IfYou",   nil];
        NSDictionary *mouthCorners = [NSDictionary dictionaryWithObjectsAndKeys:
            turnsUpMouthConers, @"Up", straightMouthConers, @"Straight", turnsDownMouthConers,@"Down", nil ];
        
        allChoices = [NSArray arrayWithObjects:
                      topOfHeard,    // 0
                      faceShape,     // 1
                      lips,          // 2
                      eyebrowShapes, // 3 
                      eyebrowHeight, // 4
                      eyeSpacing,    // 5
                      cheekbones,    // 6
                      nostrils,      // 7 
                      noseUnderside, // 8
                      chin,          // 9
                      mouthCorners,  // 10
                      nil];
        [allChoices retain];
    
    }
    return self;
}


@end
