//
//  ReadFace.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ReadFace.h"

@implementation ReadFace

@synthesize gender, topOfHead, faceShape, lips, eyebrowShape, eyebrowHeight, eyeSpacing, cheeckbones, nostrils;
@synthesize noseUnderside, chin, mouthCorners;

-(NSString *) description
{
    return[NSString stringWithFormat:@"<gender> %@ <topOfHead> %@ <faceShape> %@ <lips> %@ <eyebrowShape> %@ <eyebrowHeight> %@ <eyeSpacing> %@ <cheeckbones> %@ <nostrills> %@ <noseUnderside> %@ <chin> %@ <mouthCorners> %@ ", gender, topOfHead, faceShape, lips, eyebrowShape, eyebrowHeight, eyeSpacing, cheeckbones,nostrils, noseUnderside, chin, mouthCorners ];
}

- (void)dealloc
{
    [gender release];
    [topOfHead release];
    [faceShape release];
    [lips release];
    [eyebrowShape release];
    [eyebrowHeight release];
    
    [eyeSpacing release];
    [cheeckbones release];
    [nostrils release];
    [noseUnderside release];
    [chin release];
    [mouthCorners release];
    [super dealloc];
}


@end
