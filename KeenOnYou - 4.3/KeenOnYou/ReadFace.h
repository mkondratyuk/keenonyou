//
//  ReadFace.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReadFace : NSObject

@property (nonatomic, retain) NSString *gender;
@property (nonatomic, retain) NSString *topOfHead;
@property (nonatomic, retain) NSString *faceShape;
@property (nonatomic, retain) NSString *lips;
@property (nonatomic, retain) NSString *eyebrowShape;
@property (nonatomic, retain) NSString *eyebrowHeight;
@property (nonatomic, retain) NSString *eyeSpacing;
@property (nonatomic, retain) NSString *cheeckbones;
@property (nonatomic, retain) NSString *nostrils;
@property (nonatomic, retain) NSString *noseUnderside;
@property (nonatomic, retain) NSString *chin;
@property (nonatomic, retain) NSString *mouthCorners;


@end
