//
//  RowTextViewController.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RowTextViewController : UIViewController <UITextViewDelegate>

@property (retain, nonatomic) IBOutlet UITextView *textView;

@property (retain, nonatomic) IBOutlet UILabel *namedFeature;

@property (retain, nonatomic) NSString  *featureString;
@property (retain, nonatomic) NSString  *namedFeatureString;

@property (retain, nonatomic) IBOutlet UIButton *backImageButton;



- (IBAction)backButtonDidTap:(id)sender;

@end
