//
//  TopOfHeadViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TopOfHeadViewController.h"
#import "ReadFace.h"
#import "FaceShape.h"

@implementation TopOfHeadViewController

@synthesize currentReadFace;

@synthesize levelHeadButton;
@synthesize highFrontButton;
@synthesize highBackButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        [self.levelHeadButton setImage:[UIImage imageNamed:@"TopOfHead_Male_LevelHeaded.png"] forState:UIControlStateNormal];
        [self.highBackButton setImage:[UIImage imageNamed:@"TopOfHead_Male_HighBack.png"] forState:UIControlStateNormal];
        [self.highFrontButton setImage:[UIImage imageNamed:@"TopOfHead_Male_HighFront.png"] forState:UIControlStateNormal];
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"])
    {
        [self.levelHeadButton setImage:[UIImage imageNamed:@"TopOfHead_Female_LevelHeaded.png"] forState:UIControlStateNormal];
        [self.highBackButton setImage:[UIImage imageNamed:@"TopOfHead_Female_HighBack.png"] forState:UIControlStateNormal];
        [self.highFrontButton setImage:[UIImage imageNamed:@"TopOfHead_Female_HighFront.png"] forState:UIControlStateNormal];          
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
        
    }

}

- (void)viewDidUnload
{
    [self setLevelHeadButton:nil];
    [self setHighFrontButton:nil];
    [self setHighBackButton:nil];
    [self setTopOfHeadImageButton:nil];
    [self setHighFrontImageButton:nil];
    [self setLevelHeadImageButton:nil];
    [self setHighBackImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)levelHeadDidTap:(id)sender
{
    [Sound soundEffect:1];
    
    FaceShape *fsvc = nil;
    //
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        fsvc = [[FaceShape alloc] initWithNibName:@"FaceShapeView-iPad"
                                           bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    fsvc = [[FaceShape alloc] initWithNibName: @"FaceShape-568h"
                                                       bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    fsvc= [[FaceShape alloc] initWithNibName:@"FaceShape"
                                                      bundle:nil];
                }
            }
            else
            {
                //iphone screen
                fsvc= [[FaceShape alloc] initWithNibName:@"FaceShape"
                                                  bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    self.currentReadFace.topOfHead = @"LevelHead";
    fsvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:fsvc animated:YES];
    [fsvc release]; 
    
}

- (IBAction)highFrontDidTap:(id)sender
{
    [Sound soundEffect:1];
    //  add current ReadFace instance 
    self.currentReadFace.topOfHead = @"HighFront";
    //
    FaceShape *fsvc = nil;
    //
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        fsvc = [[FaceShape alloc] initWithNibName:@"FaceShapeView-iPad"
                                                        bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    fsvc = [[FaceShape alloc] initWithNibName: @"FaceShape-568h"
                                                       bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    fsvc= [[FaceShape alloc] initWithNibName:@"FaceShape"
                                                      bundle:nil];
                }
            }
            else
            {
                //iphone screen
                fsvc= [[FaceShape alloc] initWithNibName:@"FaceShape"
                                                  bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    //  transmission of the current value instance ReadFaceon on the chaine  
    fsvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:fsvc animated:YES];
    [fsvc release]; 
}

- (IBAction)highBackDidTap:(id)sender
{
    [Sound soundEffect:1];
    
    //  block for load variouse views (iPad & iPhone 5)
    
    FaceShape *fsvc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        fsvc = [[FaceShape alloc] initWithNibName:@"FaceShapeView-iPad"
                                           bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    fsvc = [[FaceShape alloc] initWithNibName: @"FaceShape-568h"
                                                       bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    fsvc= [[FaceShape alloc] initWithNibName:@"FaceShape"
                                                      bundle:nil];
                }
            }
            else
            {
                //iphone screen
                fsvc= [[FaceShape alloc] initWithNibName:@"FaceShape"
                                                  bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    //  end block
    
    self.currentReadFace.topOfHead = @"HighBack";
    fsvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:fsvc animated:YES];
    [fsvc release]; 
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc
{
    [levelHeadButton release];
    [highFrontButton release];
    [highBackButton release];
    [_topOfHeadImageButton release];
    [_highFrontImageButton release];
    [_levelHeadImageButton release];
    [_highBackImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark
#pragma mark customizing localiziable button or labels


- (void) localizingThroughAddingLabels
{
    // 1. Top of Head
    
    //  make image of button  nil (invisible)
    [self.topOfHeadImageButton setImage:nil  forState:UIControlStateNormal];
    [self.topOfHeadImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.topOfHeadImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Top of Head", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.topOfHeadImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.topOfHeadImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. High Front
    
    [self.highFrontImageButton  setImage:nil forState:UIControlStateNormal];
    [self.highFrontImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.highFrontImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"High Front Top of Head", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.highFrontImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.highFrontImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.highFrontImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.highFrontImageButton.center.x, self.highFrontImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Level Head
    
    [self.levelHeadImageButton setImage:nil    forState:UIControlStateNormal];
    [self.levelHeadImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.levelHeadImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Level Head Top of Head", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.levelHeadImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.levelHeadImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.levelHeadImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.levelHeadImageButton.center.x, self.levelHeadImageButton.center.y);
        }
        
    }

    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.   High Back
    
    [self.highBackImageButton setImage:nil    forState:UIControlStateNormal];
    [self.highBackImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.highBackImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"High Back Top of Head", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.highBackImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.highBackImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.highBackImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.highBackImageButton.center.x, self.highBackImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;

    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;

}


@end
