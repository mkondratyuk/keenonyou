//
//  MouthCornersViewController.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MouthCornersViewController.h"
#import "ReadFace.h"
#import "FaceReadingViewController.h"

@implementation MouthCornersViewController

@synthesize currentReadFace;

@synthesize turnsUpButton;
@synthesize straightButton;
@synthesize turnsDownButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        [self.turnsUpButton setImage:[UIImage imageNamed:@"MouthCorners_Male_TurnsUp.png"] forState:UIControlStateNormal];
        [self.straightButton setImage:[UIImage imageNamed:@"MouthCorners_Male_Straight.png"] forState:UIControlStateNormal]; 
        [self.turnsDownButton setImage:[UIImage imageNamed:@"MouthCorners_Male_TurnsDown.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"])
    {
        [self.turnsUpButton setImage:[UIImage imageNamed:@"MouthCorners_Female_TurnsUp.png"] forState:UIControlStateNormal];
        [self.straightButton setImage:[UIImage imageNamed:@"MouthCorners_Female_Straight.png"] forState:UIControlStateNormal]; 
        [self.turnsDownButton setImage:[UIImage imageNamed:@"MouthCorners_Female_TurnsDown.png"] forState:UIControlStateNormal];         
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }


}

- (void)viewDidUnload
{
    [self setTurnsUpButton:nil];
    [self setStraightButton:nil];
    [self setTurnsDownButton:nil];
    [self setMouthCornersImageButton:nil];
    [self setTurnUpImageButton:nil];
    [self setStraightImageButton:nil];
    [self setTurnsDownImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark memory management

- (void)dealloc
{
    [turnsUpButton release];
    [straightButton release];
    [turnsDownButton release];
    [_mouthCornersImageButton release];
    [_turnUpImageButton release];
    [_straightImageButton release];
    [_turnsDownImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions

-(FaceReadingViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    FaceReadingViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[FaceReadingViewController alloc] initWithNibName:@"FaceReadingView-iPad"
                                                         bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[FaceReadingViewController alloc] initWithNibName: @"FaceReadingView-568h"
                                                                      bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[FaceReadingViewController alloc] initWithNibName:@"FaceReadingViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[FaceReadingViewController alloc] initWithNibName:@"FaceReadingViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
}


- (IBAction)turnsUpButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.mouthCorners = @"Up";
    
    FaceReadingViewController *frvc= [self newTrueView];
    
    frvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:frvc animated:YES];
    [frvc release];
}    
    
- (IBAction)straightButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.mouthCorners = @"Straight";
    
    FaceReadingViewController *frvc= [self newTrueView];
    
    frvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:frvc animated:YES];
    [frvc release];   
}

- (IBAction)turnsDownButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.mouthCorners = @"Down";
    
    FaceReadingViewController *frvc= [self newTrueView];
    
    frvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:frvc animated:YES];
    [frvc release];
}


- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];    
}


#pragma mark
#pragma mark customizing localiziable labels above images ...

- (void) localizingThroughAddingLabels
{
    // 1. Mouth Corners
    
    //  make image of button  nil (invisible)
    [self.mouthCornersImageButton setImage:nil  forState:UIControlStateNormal];
    [self.mouthCornersImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.mouthCornersImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Cheekbones", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.mouthCornersImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.mouthCornersImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Turns Up Mouth Corners
    
    [self.turnUpImageButton  setImage:nil forState:UIControlStateNormal];
    [self.turnUpImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.turnUpImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Turns Up Mouth Corners", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.turnUpImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.turnUpImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.turnUpImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.turnUpImageButton.center.x, self.turnUpImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Straight Mouth Corners
    
    [self.straightImageButton setImage:nil    forState:UIControlStateNormal];
    [self.straightImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.straightImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Straight Mouth Corners", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.straightImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.straightImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.straightImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.straightImageButton.center.x, self.straightImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.  Turns Down Mouth Corners
    
    [self.turnsDownImageButton setImage:nil    forState:UIControlStateNormal];
    [self.turnsDownImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.turnsDownImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Turns Down Mouth Corners", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.turnsDownImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.turnsDownImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.turnsDownImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.turnsDownImageButton.center.x, self.turnsDownImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}

@end
