//
//  LipsViewController.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace; 

@interface LipsViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *thinAndThinButton;
@property (retain, nonatomic) IBOutlet UIButton *thinAndThickButton;
@property (retain, nonatomic) IBOutlet UIButton *thickAndThinButton;
@property (retain, nonatomic) IBOutlet UIButton *thickAndThickButton;

- (IBAction)thinAndThinButtonDidTap:(id)sender;
- (IBAction)thinAndThickButtonDidTap:(id)sender;
- (IBAction)thickAndThinButtonDidTap:(id)sender;
- (IBAction)thickAndThickButtonDidTap:(id)sender;

// for localization ...
@property (retain, nonatomic) IBOutlet UIButton *lipsImageButton;
@property (retain, nonatomic) IBOutlet UIButton *thinTopAndBottomImageButton;
@property (retain, nonatomic) IBOutlet UIButton *thinTopThickBottomImageButton;
@property (retain, nonatomic) IBOutlet UIButton *thickTopThinBottomImageButton;
@property (retain, nonatomic) IBOutlet UIButton *thickTopAndBottomImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)backButtonDidTap:(id)sender;


@end
