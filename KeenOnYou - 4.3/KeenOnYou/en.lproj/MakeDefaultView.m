//
//  MakeDefaultView.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MakeDefaultView.h"
#import "FileHelpers.h"

@implementation MakeDefaultView

@synthesize backImageView;
@synthesize logoImageVie;
@synthesize label;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self saveCurrentAvatarImageInFilePNG];
}

- (void)viewDidUnload
{
    
    [self setBackImageView:nil];
    [self setLogoImageVie:nil];
    [self setLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


- (void) saveCurrentAvatarImageInFilePNG
{
    //[backImageView addSubview:logoImageVie];
    //[backImageView addSubview:label];
    
    NSMutableString *pathname = [NSMutableString stringWithCapacity:0];
    [pathname setString:@"image"];
    [pathname appendString:@".png"];
    DLog(@" NAME OF FILE WITH .png =  %@", pathInDocumentDirectory([pathname substringFromIndex:0]));
    
    //создание контекста...
    UIGraphicsBeginImageContext(self.view.frame.size);
    
    // вывод содержимого в этот контекст
    [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // создание UIImage
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();  
    
    // cоздание NSData
    NSData *pngData = UIImagePNGRepresentation(image);
    
    //
    [pngData writeToFile:pathInDocumentDirectory([pathname substringFromIndex:0]) atomically:YES];
    // закрытие контекста
    UIGraphicsEndImageContext();
    
        
}


- (void)dealloc
{
    
    [backImageView release];
    [logoImageVie release];
    [label release];
    [super dealloc];
}
@end
