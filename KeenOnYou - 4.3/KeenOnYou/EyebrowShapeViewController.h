//
//  EyebrowShapeViewController.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface EyebrowShapeViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *semiCircleButton;
@property (retain, nonatomic) IBOutlet UIButton *thickButton;
@property (retain, nonatomic) IBOutlet UIButton *sharpPointButton;
@property (retain, nonatomic) IBOutlet UIButton *straightButton;
@property (retain, nonatomic) IBOutlet UIButton *highArcButton;

// for localization ...

@property (retain, nonatomic) IBOutlet UIButton *eyebrowShapeImageButton;
@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *semiCircleImageButton;
@property (retain, nonatomic) IBOutlet UIButton *thickImageButton;
@property (retain, nonatomic) IBOutlet UIButton *sharpPointImageButton;
@property (retain, nonatomic) IBOutlet UIButton *straightImageButton;
@property (retain, nonatomic) IBOutlet UIButton *highArcImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;


- (IBAction)averageButtonDidTap:(id)sender;
- (IBAction)semiCircleButtonDidTap:(id)sender;
- (IBAction)thickButtonDidTap:(id)sender;
- (IBAction)sharpPointButtonDidTap:(id)sender;
- (IBAction)straitPointButtonDidTap:(id)sender;
- (IBAction)highArcButtonDidTap:(id)sender;




- (IBAction)backButtonDidTap:(id)sender;


@end
