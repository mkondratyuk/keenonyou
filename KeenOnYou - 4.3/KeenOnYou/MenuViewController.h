//
//  MenuViewController.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Sound.h"

@interface MenuViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIButton *readFaceButton;
@property (retain, nonatomic) IBOutlet UIButton *faceHistoryButton;
@property (retain, nonatomic) IBOutlet UIButton *aboutUsButton;

- (IBAction)readFaceButtonDidTap:(id)sender;

- (IBAction)faceHistoryButtonDidTap:(id)sender;

- (IBAction)aboutUsButtonDidTap:(id)sender;

- (void) backFromListSavedFacesTableViewController;

- (IBAction)playMusicSwitchDidChangeValue:(id)sender;

- (IBAction)musiconOffButtonDidTap:(id)sender;

@property (retain, nonatomic) IBOutlet UISwitch *musicSwitch;
@property (retain, nonatomic) IBOutlet UIButton *musicOnOffButton;

// 
@property (retain, nonatomic) IBOutlet UIButton *readFaceTextButton;
@property (retain, nonatomic) IBOutlet UIButton *faceHistoryTextButton;
@property (retain, nonatomic) IBOutlet UIButton *aboutUsTextButton;

@end
