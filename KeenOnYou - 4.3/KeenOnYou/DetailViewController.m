//
//  DetailViewController.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DetailViewController.h"
#import "RowTextViewController.h"
#import "FileHelpers.h"
#import "ListSavedFacesTableViewController.h"
#import "QuartzCore/QuartzCore.h"

@interface DetailViewController ()

{
    NSMutableArray *namedFeatureDesc;
    NSMutableArray *featureDesc;
    
    NSMutableArray *namedFeatureIfYou;
    NSMutableArray *featureIfYou;
    
}

@end

@implementation DetailViewController

@synthesize scrollView;
@synthesize avatarImageView;
@synthesize tableView;
@synthesize deleteFaceButton;

@synthesize currentAvatarDictionary;
@synthesize nameLabel;

@synthesize indexInListOfSavedFaces;

@synthesize didPassedFromFaceReading;
@synthesize viewWithDisclaimerText;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.scrollView.contentSize = CGSizeMake(0, 850);
    
    namedFeatureDesc = [NSMutableArray arrayWithCapacity:0];
    featureDesc = [NSMutableArray arrayWithCapacity:0];
    
    namedFeatureIfYou = [NSMutableArray arrayWithCapacity:0];
    featureIfYou = [NSMutableArray arrayWithCapacity:0];
    
    // load avatar in UIImageView
    NSMutableString *nameFilePNG = [NSMutableString stringWithCapacity:0];
    NSString *name = [self.currentAvatarDictionary objectForKey:@"Name"];
    self.nameLabel.text = name;
    [nameFilePNG setString:name];
    [nameFilePNG appendString:@".png"];
    NSLog(@"nameFilePNG = %@", nameFilePNG);
    NSString *path = pathInDocumentDirectory([nameFilePNG substringFromIndex:0]);
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    [self.avatarImageView setImage:image];
    
    //
    [namedFeatureIfYou retain];
    [namedFeatureDesc retain];
    
    [featureIfYou retain];
    [featureDesc retain];
    
    

}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    double height;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        // rowHeght * N + 2 * (heghtHeader + heghtFooter)
        height = ([[currentAvatarDictionary objectForKey:@"Desc"] count] + [[currentAvatarDictionary objectForKey:@"IfYou"] count] ) * 70.0 + 140.0 + 30.0;
                
        tableView.frame = CGRectMake(9.0, 894.0, 750.0, height);
        viewWithDisclaimerText.frame = CGRectMake(0.0, 894.0 + height, 768.0, 90.0);
        
        deleteFaceButton.frame  = CGRectMake(123.0, height + 894.0 + 10.0 + 90.0 + 30.0, 522.0, 94.0);
        scrollView.contentSize = CGSizeMake(0, 894.0 + height + 200.0 + 30.0);
   
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        // iPhone
        height = ([[currentAvatarDictionary objectForKey:@"Desc"] count] + [[currentAvatarDictionary objectForKey:@"IfYou"] count] ) * 44.0 + 80.0 + 20.0;
        tableView.frame = CGRectMake(10.0, 380.0, 300.0, height);
        
        viewWithDisclaimerText.frame = CGRectMake(0.0, 380.0 + height + 10.0 + 20.0, 320.0, 100.0);
        
        deleteFaceButton.frame  = CGRectMake(49.0, height + 380.0 + 10.0 + 100.0 + 40.0, 222.0, 40.0);
        scrollView.contentSize = CGSizeMake(0, 380.0 + height + 10.0 + 100.0 + 40.0 + 10.0 + 60.0);
        
    }
    else
    {
        DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
    }
#ifndef __IPHONE_6_0
    NSDictionary *underlineAttribute = @{ NSUnderlineStyleAttributeName: @1 };
    self.howToReadFacesLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"How to Read Faces", nil)
                                                                              attributes:underlineAttribute];
#else
    
#endif

    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }
}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setTableView:nil];
    [self setAvatarImageView:nil];
    
    [self setNameLabel:nil];
    [self setDeleteFaceButton:nil];
    [self setViewWithDisclaimerText:nil];
    [self setFaceReadingImageButton:nil];
    [self setDeleteImageButton:nil];
    [self setBackImageButton:nil];
    [self setMenuImageButton:nil];
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    
    [namedFeatureDesc release];
    [featureDesc release];
    [namedFeatureIfYou release];
    [featureIfYou release];
    
    [scrollView release];
    [tableView release];
    [avatarImageView release];
    
    [nameLabel release];
    [deleteFaceButton release];
    [viewWithDisclaimerText release];
    [_howToReadFacesLabel release];
    [_faceReadingImageButton release];
    [_deleteImageButton release];
    [_backImageButton release];
    [_menuImageButton release];
    [super dealloc];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if ([[self.currentAvatarDictionary objectForKey:@"IfYou"] count] > 0) {
        return 2;
    }
    return 1;
}

- (NSString *)      tableView:(UITableView *)tableViewVar
      titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Description";
    }
    if (section == 1)
    {
        return @"If You ?";
    }
    return @"";
}


- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    if (section == 0) {
        return [[currentAvatarDictionary objectForKey:@"Desc"] count];
    }
    return [[currentAvatarDictionary objectForKey:@"IfYou"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableViewVar 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{  
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableViewVar dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle 
                                       reuseIdentifier:CellIdentifier]autorelease];
    }
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        cell.detailTextLabel.numberOfLines = 0;
        //cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
        //cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }

    
    NSArray *arrayDescForAvatar = [self.currentAvatarDictionary objectForKey:@"Desc"];
    NSArray *arrayIfYouForAvatar = [self.currentAvatarDictionary objectForKey:@"IfYou"]; 
    
    NSString *currentString = nil; 
    NSRange firstSubrange, secondSubrange;
    int length, location;
    if (indexPath.section == 0)
    {
        currentString = [arrayDescForAvatar objectAtIndex:indexPath.row];
        
    }
    if (indexPath.section == 1)
    {
        currentString = [arrayIfYouForAvatar objectAtIndex:indexPath.row];
        
    }
    firstSubrange = [currentString rangeOfString:@"<b>"];
    secondSubrange = [currentString rangeOfString:@"</b>"];
    location = firstSubrange.location + firstSubrange.length;
    length = secondSubrange.location - location;
    
    //  begin new block of code
    NSString *firstKey = [currentString substringWithRange:NSMakeRange(location, length)];
    NSString *secondKey = [firstKey stringByAppendingString:@"_DESC"];
    NSString *englishDescription = [currentString substringFromIndex:(secondSubrange.location + secondSubrange.length + 1)];
    DLog(@" firstKey: %@ secondKey: %@  englishDescription: %@", firstKey, secondKey, englishDescription);
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            cell.textLabel.text =  NSLocalizedString(firstKey, nil);
            cell.detailTextLabel.text = NSLocalizedString(secondKey, nil);
        }
        else
        {
            cell.textLabel.text =  firstKey;
            cell.detailTextLabel.text = englishDescription;
        }
    }
    else
    {
        cell.textLabel.text =  firstKey;
        cell.detailTextLabel.text = englishDescription;
    }
    
    DLog(@" %@",cell.textLabel.text );
    DLog(@" %@", cell.detailTextLabel.text);
    
    //  end new block of code
    
    if (indexPath.section == 0)
    {
        [namedFeatureDesc  insertObject:cell.textLabel.text atIndex:indexPath.row];
        [featureDesc  insertObject:cell.detailTextLabel.text atIndex:indexPath.row]; 
    }
    
    if (indexPath.section == 1)
    {
        [namedFeatureIfYou  insertObject:cell.textLabel.text atIndex:indexPath.row];
        [featureIfYou  insertObject:cell.detailTextLabel.text atIndex:indexPath.row];        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.backgroundColor = [UIColor clearColor];
    //cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return;
    }
    
    [Sound soundEffect:1];
    
    RowTextViewController *rvc = nil;
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        rvc = [[RowTextViewController alloc] initWithNibName:@"RowTextView-568h" bundle:nil];
    }
    else
    {
        rvc = [[RowTextViewController alloc] initWithNibName:@"RowTextViewController" bundle:nil];
    } 
    
    if (indexPath.section == 0)
    {
        rvc.featureString = [featureDesc objectAtIndex:indexPath.row] ;
        rvc.namedFeatureString = [namedFeatureDesc objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 1)
    {
        rvc.featureString = [featureIfYou objectAtIndex:indexPath.row] ;
        rvc.namedFeatureString = [namedFeatureIfYou objectAtIndex:indexPath.row];
    }
    
    [self.navigationController pushViewController:rvc animated:YES];
    [rvc release];
}

#pragma mark hearder view in tableview delegate !!!


- (CGFloat)        tableView:(UITableView *)tableView_
    heightForHeaderInSection:(NSInteger)section

{
    CGFloat height = 10.0;
    if (tableView_ == self.tableView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            height = 61;
            //cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
        }
        else
        {
            height = 30.0;
        }

    }
    return height;
}


- (UIView *)    tableView:(UITableView *)tableView_
   viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    
    if (tableView_ == self.tableView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            // iPad
            if (section == 0)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Description.png"]];
                view = descriptionView;
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  Description
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"Description", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: 47.0];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }
                
            }
            if (section == 1)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IfYouTry.png"]];
                view = descriptionView;
                // localizing ...
                
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  If You? Try…
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"If You? Try…", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: 47.0];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }
                
            }
            
        }
        else
        {
            // iPhone
            if (section == 0)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Description-iPhone.png"]];
                view = descriptionView;
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  Description
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"Description", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: view.frame.size.height];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }
            }
            
            if (section == 1)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IfYouTry-iPhone.png"]];
                view = descriptionView;
                // localizing ...
                
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  If You? Try…
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"If You? Try…", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: view.frame.size.height];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }
                
            }
        }
        
    }
    
    return [view autorelease];
    
}


#pragma mark  IB actions

- (IBAction)backButtobDidTap:(id)sender 
{   
    [Sound soundEffect:1];
    if (didPassedFromFaceReading == YES)
    {
    // обеспечение реагирования на запрос на переход в главное меню путем оповещения
       
        NSNotification *notification = [NSNotification 
                                        notificationWithName:@"BackToMenu"
                                        object:self 
                                        userInfo:nil];
        [[NSNotificationCenter defaultCenter] postNotification:notification]; 
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)menuButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)deleteFaceDidTap:(id)sender
{
    [Sound soundEffect:0];
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Confirm deleting Item", nil)
                                                    message:NSLocalizedString(@"Are you sure?", nil)
                                                   delegate:self 
                                          cancelButtonTitle:NSLocalizedString(@"NO", nil)
                                          otherButtonTitles:NSLocalizedString(@"Delete", nil) , nil];
	[alert show];
	[alert release];
}

- (IBAction)howToReadFacesDidTap:(id)sender
{
    DLog(@" ");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.keenonyou.com/How-To-Read-Faces.php"]];
    
}

#pragma mark uialertview delegate

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex
{
    DLog(@"The index of the alert button clicked is %d", buttonIndex);
    if (buttonIndex == 0)
    {
        return;
    }
    if (buttonIndex == 1)
    {
        NSString *fileNameListOfSavedFaces = pathInDocumentDirectory(@"listsavedfaces");
        NSMutableArray *listOfSavedFaces = [NSMutableArray arrayWithContentsOfFile:fileNameListOfSavedFaces];
        // find number in array (search index in the array from file name in the dictionary ...)
        NSUInteger trueIndexInListOfSavedFaces = 0;
        for (NSDictionary *avatarDictionary in listOfSavedFaces)
        {
            if ([[avatarDictionary objectForKey:@"Name"] isEqualToString:[self.currentAvatarDictionary objectForKey:@"Name"]] )
            {
                break;
            }
            trueIndexInListOfSavedFaces++;
        }
        [listOfSavedFaces removeObjectAtIndex:trueIndexInListOfSavedFaces];
        
        if ([listOfSavedFaces writeToFile:fileNameListOfSavedFaces atomically:YES] == NO)
        {
            DLog(@"Save to file failed!!! ");
        }
        //delete png files ... 
        NSFileManager *fm = [NSFileManager defaultManager];
        NSMutableString *nameFilePNG = [NSMutableString stringWithCapacity:0];
        [nameFilePNG setString:self.nameLabel.text];
        [nameFilePNG appendString:@".png"];
        NSLog(@"nameFilePNG = %@", nameFilePNG);
        NSString *path = pathInDocumentDirectory([nameFilePNG substringFromIndex:0]);
        if ([fm removeItemAtPath:path error:NULL] == NO)
        {
            DLog(@"file removal failed !!!");
        }
        [nameFilePNG insertString:@"small" atIndex:0];
        NSString *pathT = pathInDocumentDirectory([nameFilePNG substringFromIndex:0]);
        if ([fm removeItemAtPath:pathT error:NULL] == NO)
        {
            DLog(@"file removal failed !!!");
        }
        //
        [self backButtobDidTap:nil];
    }
    
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Face Reading
    
    //  make image of button  nil (invisible)
    [self.faceReadingImageButton setImage:nil  forState:UIControlStateNormal];
    [self.faceReadingImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    CGRect faceReadingFrame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        faceReadingFrame = CGRectMake(155, 20, 459, 56);
    }
    else
    {
        faceReadingFrame = CGRectMake(20, 20, 280, 35);
    }
    label = [[UILabel alloc] initWithFrame:faceReadingFrame];
    label.backgroundColor = [UIColor clearColor];
    //DLog(@"Face Reading localized value: %@", NSLocalizedString(@"Face Reading", nil));
    label.text = NSLocalizedString(@"Face Reading", nil);
    
    label.font = [UIFont boldSystemFontOfSize: self.faceReadingImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.textAlignment = NSTextAlignmentCenter;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label.center = CGPointMake(384, 58);
    }
    else
    {
        label.center = CGPointMake(160, 35);
    }
    
    [self.scrollView addSubview:label];
    [label release];
    label = nil;
    
    // 2. Delete
    
    [self.deleteFaceButton  setImage:nil forState:UIControlStateNormal];
    [self.deleteFaceButton setTitle: @" " forState:UIControlStateNormal];
    [self.deleteFaceButton setBackgroundColor:[UIColor redColor]];
    
    // handsom textVie
    self.deleteFaceButton.layer.borderWidth = 2;
    self.deleteFaceButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.deleteFaceButton.layer.cornerRadius = 16;
    
    UILabel *label2 = nil;
    
    label2 = [[UILabel alloc] initWithFrame:self.deleteFaceButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Delete", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.deleteFaceButton.frame.size.height -10.0];
    label2.textColor = [UIColor yellowColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    //label2.textAlignment = NSTextAlignmentCenter;
    label2.center = self.deleteFaceButton.center;
    [self.scrollView addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3. Menu
    
    [self.menuImageButton setImage:nil forState:UIControlStateNormal];
    [self.menuImageButton setTitle:@" " forState:UIControlStateNormal];
    CGRect frame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
        if (![currentLanguage isEqualToString:@"en"])
        {
            if ([currentLanguage isEqualToString:@"ja"])
            {
                frame = CGRectMake(560.0, 946.0, 123.0, 47.0);
                CGRect menuButtonFrame = self.menuImageButton.frame;
                menuButtonFrame = CGRectMake(menuButtonFrame.origin.x - 80.0 , menuButtonFrame.origin.y, menuButtonFrame.size.width + 80.0, menuButtonFrame.size.height);
                self.menuImageButton.frame = menuButtonFrame;
            }
            if ([currentLanguage isEqualToString:@"zh-Hans"])
            {
                frame = CGRectMake(640.0, 946.0, 123.0, 47.0);
            }
        }
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(255, 507, 82, 31);
        }
        else
        {
            frame = CGRectMake(255, 417, 82, 31);
        }
    }
    
    
    UILabel *labelMenu = [[UILabel alloc] initWithFrame:frame];
    labelMenu.backgroundColor = [UIColor clearColor];
    labelMenu.text = NSLocalizedString(@"Menu", nil);;
    labelMenu.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    labelMenu.textColor = [UIColor redColor];
    labelMenu.shadowColor = [UIColor lightGrayColor];
    labelMenu.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [labelMenu sizeToFit];
    //label4.center = frame.center;
    [self.view addSubview:labelMenu];
    [labelMenu release];
    labelMenu = nil;
    
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        frame = CGRectMake(91, 946, 123, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(60, 507, 82, 31);
        }
        else
        {
            frame = CGRectMake(60, 417, 82, 31);
        }
    }
    
    
    
    UILabel *label4 = [[UILabel alloc] initWithFrame:frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    //
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}



@end
