//
//  MenuViewController.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MenuViewController.h"
#import "ListSavedFacesTableViewController.h"
#import "AboutUsViewController.h"

#import "GenderViewController.h"
#import "ChoisesStore.h"
#import "MakeDefaultView.h"
#import "AppDelegate.h"

@implementation MenuViewController

@synthesize musicSwitch = _musicSwitch;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark
#pragma mark localization in life !!!

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    NSString *labelFormat = NSLocalizedString(@"main language: %@", nil);
    NSString *textForLabel = [NSString stringWithFormat:labelFormat, currentLanguage];
    DLog(@"%@", textForLabel);
    textForLabel = nil;
}


#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // it's for making default images
    
    //[self addMyButton];
    
    // Do any additional setup after loading the view from its nib.
    DLog(@"WE ARE IN THERE");
    [self.navigationController.navigationBar setHidden:YES];
    // Controll of input 
    NSArray *arrayOfChoises = [[ChoisesStore defaultStore] allChoices];
    
    for (NSDictionary *dictionary in arrayOfChoises)
    {
       // DLog(@" Menu Choice = %@", dictionary);
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector: @selector(backFromListSavedFacesTableViewController)
                                                 name: @"BackToMenu" 
                                               object: nil];
    /*
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector: @selector(handleKeenOnYouNotificationSettingsDidUpdate:)
                                                 name: KeenOnYouNotificationSettingsDidUpdate
                                               object: nil];
     */
    
    [[NSNotificationCenter defaultCenter]     addObserver:self
                                                 selector:@selector(settingsChanged:)
                                                     name:NSUserDefaultsDidChangeNotification
                                                   object:nil];
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    
    }
}





- (void) viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
        
    //[[NSNotificationCenter defaultCenter] removeObserver:self];

}


- (void)viewDidUnload
{
    
    [self setMusicSwitch:nil];
    [self setReadFaceTextButton:nil];
    [self setFaceHistoryTextButton:nil];
    [self setAboutUsTextButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void) viewWillAppear:(BOOL)animated
{
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagOfMusic = [standardUserDefaults boolForKey:@"MusicInApp"];
    [self.musicSwitch setOn:flagOfMusic animated:YES];
    AVAudioPlayer *player = [(AppDelegate*)[[UIApplication sharedApplication]  delegate] player];
    
    if (flagOfMusic == YES)
    {
        [player play];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOn-iPad.png"]
                                   forState:UIControlStateNormal];
        }
        else
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOn.png"]
                                   forState:UIControlStateNormal];
        }
        //DLog(@"we there, self.musicSwitch = %d  ON", flagOfMusic);
    }
    else
    {
        [player pause];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOff-iPad.png"]
                                   forState:UIControlStateNormal];
        }
        else
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOff.png"]
                                   forState:UIControlStateNormal];
        }
        //DLog(@"we there, self.musicSwitch = %d  OFF", flagOfMusic);
    }

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark handling notifications
/*
-(void)handleKeenOnYouNotificationSettingsDidUpdate:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    // NSDictionary *userInfo = @{@"FlagOfMusic":[NSNumber numberWithBool:flagOfMusic]};
    NSNumber *flagOfMusicWrapper  = (NSNumber*)[userInfo objectForKey:@"FlagOfMusic"];
    BOOL flagOfMusic = [flagOfMusicWrapper boolValue];
    [self.musicSwitch setOn:flagOfMusic animated:YES];
}
*/

- (void) settingsChanged:(NSNotification *)notification
{
    /*
    AVAudioPlayer *player = [(AppDelegate*)[[UIApplication sharedApplication]  delegate] player];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagOfMusic = [standardUserDefaults boolForKey:@"MusicInApp"];
    [self.musicSwitch setOn:flagOfMusic animated:YES];
    
    if (flagOfMusic)
    {
        [player play];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOn-iPad.png"]
                                   forState:UIControlStateNormal];
        }
        else
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOn.png"]
                                   forState:UIControlStateNormal];
        }
        
        //DLog(@"we there, self.musicSwitch = %d  ON", flagOfMusic);
    }
    else
    {
        [player pause];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOff-iPad.png"]
                                   forState:UIControlStateNormal];
        }
        else
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOff.png"]
                                   forState:UIControlStateNormal];
        }

        //DLog(@"we there, self.musicSwitch = %d  OFF", flagOfMusic);
    }

    */
    [self viewWillAppear:YES];
}

-(void)    backFromListSavedFacesTableViewController
{
    ListSavedFacesTableViewController *lvc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lvc = [[ListSavedFacesTableViewController alloc] initWithNibName: @"ListSavedFacesTableView-iPad"
                                                                  bundle: nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableView-568h" bundle:nil];
                }
                else
                {
                    //iphone retina screen
                    lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" WHAT THE MATTER WITH USER_INTERFACE_IDIOM ?");
        }
    }
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
    [self.navigationController pushViewController:lvc animated:NO]
    ;
    [lvc release];
    
}

#pragma mark - memory management

- (void)dealloc
{
    
    [_musicSwitch release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];

    [_readFaceButton release];
    [_faceHistoryButton release];
    [_aboutUsButton release];
    [_musicOnOffButton release];
    [_readFaceTextButton release];
    [_faceHistoryTextButton release];
    [_aboutUsTextButton release];
    [super dealloc];
}

#pragma mark IB Actions in use !!! 
 
- (IBAction)readFaceButtonDidTap:(id)sender
{
   /* 
    ReadFacePlainViewController *rfvc = [[ReadFacePlainViewController alloc] initWithNibName:@"ReadFacePlainViewController" bundle:nil];
    [self.navigationController pushViewController:rfvc animated:YES];
    [rfvc release];
    */
    [Sound soundEffect:1];
    GenderViewController *gvc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        gvc = [[GenderViewController alloc] initWithNibName:@"GenderView-iPad" bundle:nil];

    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    gvc = [[GenderViewController alloc] initWithNibName: @"GenderView-568h"
                                                                 bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    gvc = [[GenderViewController alloc] initWithNibName: @"GenderViewController"
                                                                 bundle: nil];
                }
            }
            else
            {
                //iphone screen
                gvc = [[GenderViewController alloc] initWithNibName: @"GenderViewController"
                                                             bundle: nil];
            }
            
            
        }
        else
        {
            DLog(@" ");
        }
    }

    gvc.loadFromMenuViewController = YES;
    [self.navigationController pushViewController:gvc animated:YES];
    [gvc release];    
}

- (IBAction)faceHistoryButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    ListSavedFacesTableViewController *lvc = nil;
    
    //GenderViewController *gvc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableView-iPad" bundle:nil]; 
        
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableView-568h" bundle:nil];
                }
                else
                {
                    //iphone retina screen
                    lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                lvc = [[ListSavedFacesTableViewController alloc] initWithNibName:@"ListSavedFacesTableViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
  
    [self.navigationController pushViewController:lvc animated:YES];
    [lvc release];
}

- (IBAction)aboutUsButtonDidTap:(id)sender 
{
    [Sound soundEffect:1];
    AboutUsViewController *avc = nil;
    //= [[AboutUsViewController alloc] initWithNibName:@"AboutUsViewController" bundle:nil];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        avc = [[AboutUsViewController alloc] initWithNibName:@"AboutUsView-iPad" bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    avc = [[AboutUsViewController alloc] initWithNibName: @"AboutUsView-568h"
                                                                  bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    avc = [[AboutUsViewController alloc] initWithNibName: @"AboutUsViewController"
                                                                  bundle: nil];
                }
            }
            else
            {
                //iphone screen
                avc = [[AboutUsViewController alloc] initWithNibName: @"AboutUsViewController"
                                                              bundle: nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    [self.navigationController pushViewController:avc animated:YES]
    ;
    //[self.navigationController.toolbar setHidden:NO];
    
    [avc release];
}



- (IBAction)playMusicSwitchDidChangeValue:(id)sender
{
    UISwitch *musicSwitch = (UISwitch *)sender;
    AVAudioPlayer *player = [(AppDelegate*)[[UIApplication sharedApplication]  delegate] player];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];

    if (musicSwitch.on)
    {
        [standardUserDefaults setBool:YES
                               forKey:@"MusicInApp"];
        [standardUserDefaults synchronize];

        [player play];
    }
    else
    {
        [standardUserDefaults setBool:NO
                               forKey:@"MusicInApp"];
        [standardUserDefaults synchronize];

        [player pause];
    }
    
}

- (IBAction)musiconOffButtonDidTap:(id)sender
{
    //UIButton *musicSwitch = (UIButton *)sender;
    AVAudioPlayer *player = [(AppDelegate*)[[UIApplication sharedApplication]  delegate] player];
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagOfMusic = [standardUserDefaults boolForKey:@"MusicInApp"];
    if (flagOfMusic == YES)
    {
        [player pause];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOff-iPad.png"]
                                   forState:UIControlStateNormal];
        }
        else
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOff.png"]
                                   forState:UIControlStateNormal];
        }
        
        
        [standardUserDefaults setBool:NO
                               forKey:@"MusicInApp"];
        [standardUserDefaults synchronize];

    }
    else
    {
        [player play];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOn.png"]
                                   forState:UIControlStateNormal];
        }
        else
        {
            [self.musicOnOffButton setImage:[UIImage imageNamed:@"SoundOn.png"]
                                   forState:UIControlStateNormal];
        }

        [standardUserDefaults setBool:YES
                               forKey:@"MusicInApp"];
        [standardUserDefaults synchronize];
    
    }
    
}

#pragma mark
#pragma mark customizing localiziable button or labels

- (void) localizingThroughButtons
{
    //  make image of button  nil (invisible)
    [self.readFaceTextButton setImage:nil
                             forState:UIControlStateNormal];
    NSString * title = nil;
    //  add to button text of title
    title = NSLocalizedString(@"Read Face", nil);
    [self.readFaceTextButton setTitle:title
                             forState:UIControlStateNormal];
    [self.readFaceTextButton setTitleColor:[UIColor redColor]
                                  forState:UIControlStateNormal];
    [self.readFaceTextButton setTitleShadowColor:[UIColor blackColor]
                                        forState:UIControlStateNormal];
    
    //  make image of button  nil (invisible)
    [self.faceHistoryTextButton setImage:nil
                                forState:UIControlStateNormal];
    
    //  add to button text of title
    title = NSLocalizedString(@"Face History", nil);
    [self.faceHistoryTextButton setTitle:title
                                forState:UIControlStateNormal];
    [self.faceHistoryTextButton setTitleColor:[UIColor redColor]
                                     forState:UIControlStateNormal];
    [self.faceHistoryTextButton setTitleShadowColor:[UIColor blackColor]
                                           forState:UIControlStateNormal];
    
    //  make image of button  nil (invisible)
    [self.aboutUsTextButton setImage:nil
                            forState:UIControlStateNormal];
    
    //  add to button text of title
    title = NSLocalizedString(@"About Us", nil);
    [self.aboutUsTextButton setTitle:title
                            forState:UIControlStateNormal];
    [self.aboutUsTextButton setTitleColor:[UIColor redColor]
                                 forState:UIControlStateNormal];
    [self.aboutUsTextButton setTitleShadowColor:[UIColor blackColor]
                                       forState:UIControlStateNormal];
}

- (void) localizingThroughAddingLabels
{
    // 1.   Read Face
    
    //  make image of button  nil (invisible)
    [self.readFaceTextButton setImage:nil  forState:UIControlStateNormal];
    //[self.readFaceTextButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.readFaceTextButton.frame];
    //label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Read Face", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.readFaceTextButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.readFaceTextButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2.   Face History
    //
    [self.faceHistoryTextButton setImage:nil
                                forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.faceHistoryTextButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Face History", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.faceHistoryTextButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    label2.center = self.faceHistoryTextButton.center;
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.  About Us
    
    [self.aboutUsTextButton setImage:nil
                            forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.aboutUsTextButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"About Us", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.aboutUsTextButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    label3.center = self.aboutUsTextButton.center;
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
}

#pragma mark make default image ivoker

- (void)addMyButton
{    // Method for creating button, with background image and other properties
    
    UIButton *playButton = [[UIButton buttonWithType:UIButtonTypeRoundedRect] retain];
    playButton.frame = CGRectMake(80.0, 360.0, 200.0, 30.0);
    [playButton setTitle:@"Go Make Default Image" forState:UIControlStateNormal];
    playButton.backgroundColor = [UIColor clearColor];
    [playButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal ];
    //UIImage *buttonImageNormal = [UIImage imageNamed:@"blueButton.png"];
    //UIImage *strechableButtonImageNormal = [buttonImageNormal stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    //[playButton setBackgroundImage:strechableButtonImageNormal forState:UIControlStateNormal];
    //UIImage *buttonImagePressed = [UIImage imageNamed:@"whiteButton.png"];
    //UIImage *strechableButtonImagePressed = [buttonImagePressed stretchableImageWithLeftCapWidth:12 topCapHeight:0];
    //[playButton setBackgroundImage:strechableButtonImagePressed forState:UIControlStateHighlighted];
    
    [playButton addTarget:self action:@selector(goButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:playButton];
}


-(void) goButtonDidTap:(id) sender
{
    MakeDefaultView *vc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[MakeDefaultView alloc] initWithNibName:@"MakeDefaultView-iPad" bundle:nil];
        
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[MakeDefaultView alloc] initWithNibName: @"MakeDefaultView-568h"
                                                                 bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[MakeDefaultView alloc] initWithNibName: @"MakeDefaultView"
                                                                 bundle: nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[MakeDefaultView alloc] initWithNibName: @"MakeDefaultView"
                                                             bundle: nil];
            }
            
            
        }
        else
        {
            DLog(@" ");
        }
    }
    
    //vc.loadFromMenuViewController = YES;
    [self.navigationController pushViewController:vc animated:YES];
    [vc release];

}

@end

/*
 
//
//  load true image for
//
CGFloat scale = [[UIScreen mainScreen] scale];
if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
{
    if (scale > 1.0)
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            [self.readFaceButton setImage:[UIImage imageNamed:@"face_read-90x90.png"]
                                 forState:UIControlStateNormal];
            //[self.faceHistoryButton setImage:[UIImage imageNamed:@"TopOfHead_Male_LevelHeaded.png"]
            //                     forState:UIControlStateNormal];
            //[self.aboutUsButton setImage:[UIImage imageNamed:@"TopOfHead_Male_LevelHeaded.png"]
            //                     forState:UIControlStateNormal];
        }
        else
        {
            
        }
    }
    else
    {
        
    }
}
else
{
    if (scale > 1.0)
    {
        
    }
    else
    {
        [self.readFaceButton setImage:[UIImage imageNamed:@"face_read-90x90.png"]
                             forState:UIControlStateNormal];
        // [self.faceHistoryButton setImage:[UIImage imageNamed:@"TopOfHead_Male_LevelHeaded.png"]                                    forState:UIControlStateNormal];
        // [self.aboutUsButton setImage:[UIImage imageNamed:@"TopOfHead_Male_LevelHeaded.png"]                                forState:UIControlStateNormal];
    }
}

 */
