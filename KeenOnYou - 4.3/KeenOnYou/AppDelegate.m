//
//  AppDelegate.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "MenuViewController.h"
#import <Foundation/Foundation.h>
// for make default image file

#import "MakeDefaultView.h"

@interface AppDelegate ()

- (void) playFileFromMainBundleWithName: (NSString*) name 
                           andExtention: (NSString *) extention;
@end


@implementation AppDelegate

@synthesize window = _window;
@synthesize player;


#pragma mark
#pragma mark    memory management

- (void)dealloc
{
    [_window release];
    [super dealloc];
}

#pragma mark
#pragma mark    UIApplicationDelegate methods ...


- (BOOL)application:(UIApplication *)application 
didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[AVAudioSession sharedInstance] setCategory:
     AVAudioSessionCategoryAmbient error: NULL];
    
    [self  playFileFromMainBundleWithName: @"Background_Music_loop"
                             andExtention: @"mp3"];
    self.player.numberOfLoops = -1;
    [self.player pause];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    [standardUserDefaults setBool:NO
                           forKey:@"MusicInApp"];
    [standardUserDefaults synchronize];
    //BOOL flagOfMusic = [standardUserDefaults objectForKey:@"MusicInApp"];
    if (NO)
    {
        [self.player play];
    }
    else
    {
        [self.player pause];
    }
   
    // sleep(2);
    // or
    [NSThread sleepForTimeInterval:2.0];
    
    CGRect mainScreenFrame =[[UIScreen mainScreen] bounds];
    self.window = [[[UIWindow alloc] initWithFrame:mainScreenFrame] autorelease];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    
    UINavigationController *nc = nil;
    MenuViewController *mvc = nil;
    MakeDefaultView *mdv = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0)
        {
            //iPad retina screen
            mvc = [[MenuViewController alloc] initWithNibName: @"MenuView-iPad"
                                                       bundle: nil];
        }
        else
        {
            //iPad screen or iPad mini
            mvc = [[MenuViewController alloc] initWithNibName: @"MenuView-iPad"
                                                       bundle: nil];
        }
        mdv = [[MakeDefaultView alloc] initWithNibName:@"MakeDefaultView-iPad" bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if(mainScreenFrame.size.height == 568)
                {
                    //iphone 5
                    mvc = [[MenuViewController alloc] initWithNibName: @"MenuView-568h"
                                                               bundle: nil];
                    
                }
                else
                {
                    //iphone retina screen
                    mvc= [[MenuViewController alloc] initWithNibName: @"MenuViewController"
                                                              bundle: nil];
                }
            }
            else
            {
                //iphone screen
                mvc= [[MenuViewController alloc] initWithNibName:@"MenuViewController"
                                                          bundle:nil];
            }
            
        }
        else
        {
            DLog(@" ");
        }
    }
    // make default view ...
    // nc = [[UINavigationController alloc] initWithRootViewController: mdv];

    nc = [[UINavigationController alloc] initWithRootViewController: mvc];
    [mvc autorelease];
    [mdv autorelease];
    
    self.window.rootViewController = nc;
    [nc autorelease];
    //
    [self.window makeKeyAndVisible];
    //  
     return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
    
       
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    
    [[AVAudioSession sharedInstance] setActive: YES error: NULL];
    
    DLog(@" %@", [self appNameAndVersionNumberDisplayString]);
    [self applicationDidFinishLaunching:nil];
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark    methods for play music

- (void) playFileFromMainBundleWithName: (NSString*) name 
                           andExtention: (NSString *) extention
{
    
    NSURL* backGroundMusicPath = [[NSBundle mainBundle] URLForResource:name withExtension:extention];
    //DLog(@"backGroundMusicPath = %@", backGroundMusicPath);
    NSError* err = nil;
    AVAudioPlayer *newPlayer =  [[AVAudioPlayer alloc] initWithContentsOfURL: backGroundMusicPath error: &err];
    // error-checking omitted
    //[backGroundMusicPath release];
    self.player = newPlayer; // retain policy
    [newPlayer release];
    [self.player prepareToPlay];
    [self.player setDelegate: (id)self];
    [self.player play];
}


#pragma mark  conform AVAudioSessionDelegate

- (void)endInterruption {
    AudioSessionSetActive(true);
}


#pragma mark notification handle methods

- (void) settingsChanged:(NSNotification *)notification
{
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    BOOL flagOfMusic = [standardUserDefaults boolForKey:@"MusicInApp"];
    if (flagOfMusic)
    {
        DLog(@"we there, flag of music = %d Music is playing", flagOfMusic);
        [self.player play];
    }
    else
    {
        DLog(@"we there, flag of music = %d Music is silent", flagOfMusic);
        [self.player pause];
    }
    
}

#pragma mark retrieve name and version of our app 

- (NSString *)appNameAndVersionNumberDisplayString
{
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    NSString *appDisplayName = [infoDictionary objectForKey:@"CFBundleDisplayName"];
    NSString *majorVersion = [infoDictionary objectForKey:@"CFBundleShortVersionString"];
    NSString *minorVersion = [infoDictionary objectForKey:@"CFBundleVersion"];
    
    return [NSString stringWithFormat:@"%@, Version %@ (%@)", appDisplayName, majorVersion, minorVersion];
}


#pragma mark
#pragma mark localization in life !!!

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    NSString *labelFormat = NSLocalizedString(@"main language: %@", nil);
    NSString *textForLabel = [NSString stringWithFormat:labelFormat, currentLanguage];
    DLog(@"%@", textForLabel);
}

@end
