//
//  AboutUsViewController.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutUsViewController : UIViewController

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

//
@property (retain, nonatomic) IBOutlet UIButton *aboutUsImageButton;
@property (retain, nonatomic) IBOutlet UITextView *aboutUsTextView;
@property (retain, nonatomic) IBOutlet UILabel *weLabel;
@property (retain, nonatomic) IBOutlet UILabel *tradeMarkLabel;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)backButtonDidTap:(id)sender;
- (IBAction)logoDidTap:(id)sender;

@end
