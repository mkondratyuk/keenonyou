//
//  LipsViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "LipsViewController.h"
#import "ReadFace.h"
#import "EyebrowShapeViewController.h"

@implementation LipsViewController

@synthesize currentReadFace;

@synthesize thinAndThinButton;
@synthesize thinAndThickButton;
@synthesize thickAndThinButton;
@synthesize thickAndThickButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    if ([self.currentReadFace.gender isEqualToString:@"Mail"]) {
        [self.thinAndThinButton setImage:[UIImage imageNamed:@"Lips_Male_ThinTopandBottom.png"] forState:UIControlStateNormal];
        [self.thinAndThickButton setImage:[UIImage imageNamed:@"Lips_Male_ThinTop-ThickBottom.png"] forState:UIControlStateNormal];
        [self.thickAndThinButton setImage:[UIImage imageNamed:@"Lips_Male_ThickTop-ThinBottom.png"] forState:UIControlStateNormal];
        [self.thickAndThickButton setImage:[UIImage imageNamed:@"Lips_Male_ThickTopAndBottom.png"] forState:UIControlStateNormal];
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"]) {        
        [self.thinAndThinButton setImage:[UIImage imageNamed:@"Lips_Male_ThinTopandBottom.png"] forState:UIControlStateNormal];
        [self.thinAndThickButton setImage:[UIImage imageNamed:@"Lips_Male_ThinTop-ThickBottom.png"] forState:UIControlStateNormal];
        [self.thickAndThinButton setImage:[UIImage imageNamed:@"Lips_Male_ThickTop-ThinBottom.png"] forState:UIControlStateNormal];
        [self.thickAndThickButton setImage:[UIImage imageNamed:@"Lips_Female_ThickTopAndBottom.png"] forState:UIControlStateNormal];       
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
        
    }
    
}

- (void)viewDidUnload
{
    [self setThinAndThinButton:nil];
    [self setThinAndThickButton:nil];
    [self setThickAndThinButton:nil];
    [self setThickAndThickButton:nil];
    
    [self setLipsImageButton:nil];
    [self setThinTopAndBottomImageButton:nil];
    [self setThinTopThickBottomImageButton:nil];
    [self setThickTopThinBottomImageButton:nil];
    [self setThickTopAndBottomImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [thinAndThinButton release];
    [thinAndThickButton release];
    [thickAndThinButton release];
    [thickAndThickButton release];
    
    [_lipsImageButton release];
    [_thinTopAndBottomImageButton release];
    [_thinTopThickBottomImageButton release];
    [_thickTopThinBottomImageButton release];
    [_thickTopAndBottomImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark IB Action

-(EyebrowShapeViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    EyebrowShapeViewController *esvc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        esvc = [[EyebrowShapeViewController alloc] initWithNibName:@"EyebrowShapeView-iPad"
                                                            bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    esvc = [[EyebrowShapeViewController alloc] initWithNibName: @"EyebrowShapeView-568h"
                                                                        bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    esvc = [[EyebrowShapeViewController alloc] initWithNibName:@"EyebrowShapeViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                esvc = [[EyebrowShapeViewController alloc] initWithNibName:@"EyebrowShapeViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return esvc;

}


- (IBAction)thinAndThinButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.lips = @"ThinAndThin";
    
    //  block for load variouse views (iPad & iPhone 5)
    EyebrowShapeViewController *esvc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        esvc = [[EyebrowShapeViewController alloc] initWithNibName:@"EyebrowShapeView-iPad"
                                           bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    esvc = [[EyebrowShapeViewController alloc] initWithNibName: @"EyebrowShapeView-568h" //EyebrowShapeView-568h.xib
                                                       bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    esvc = [[EyebrowShapeViewController alloc] initWithNibName:@"EyebrowShapeViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                esvc = [[EyebrowShapeViewController alloc] initWithNibName:@"EyebrowShapeViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release]; 
}

- (IBAction)thinAndThickButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.lips = @"ThinAndThick";
    
    EyebrowShapeViewController *esvc = [self newTrueView];
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release]; 
}

- (IBAction)thickAndThinButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.lips = @"ThickAndThin";
    EyebrowShapeViewController *esvc= [self newTrueView];
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release]; 
}

- (IBAction)thickAndThickButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.lips = @"ThickAndThick";
    EyebrowShapeViewController *esvc= [self newTrueView];
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release];     
}    

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}
    
#pragma mark
#pragma mark customizing localiziable button or labels


- (void) localizingThroughAddingLabels
{
    CGSize fontSize;
    // 1. Lips
    
    //  make image of button  nil (invisible)
    [self.lipsImageButton setImage:nil  forState:UIControlStateNormal];
    [self.lipsImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.lipsImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Lips", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.lipsImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.lipsImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Thin top and bottom
    
    [self.thinTopAndBottomImageButton  setImage:nil forState:UIControlStateNormal];
    [self.thinTopAndBottomImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.thinTopAndBottomImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Thin top and bottom Lips", nil);
    fontSize = self.thinTopAndBottomImageButton.frame.size;
    CGFloat height = fontSize.height;
    //DLog(@"height of font: %f", height);
    label2.font = [UIFont boldSystemFontOfSize: height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.thinTopAndBottomImageButton.center;
    }
    else
    {
        label2.font = [UIFont boldSystemFontOfSize: 25];
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.thinTopAndBottomImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.thinTopAndBottomImageButton.center.x, self.thinTopAndBottomImageButton.center.y + 5);
            
        }
        
    }
    label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Thin top / Thick bottom
    
    [self.thinTopThickBottomImageButton setImage:nil    forState:UIControlStateNormal];
    [self.thinTopThickBottomImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.thinTopThickBottomImageButton .frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Thin top / Thick bottom Lips", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.thinTopThickBottomImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.thinTopThickBottomImageButton.center;
    }
    else
    {
        label3.font = [UIFont boldSystemFontOfSize: 25];
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.thinTopThickBottomImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.thinTopThickBottomImageButton.center.x, self.thinTopThickBottomImageButton.center.y + 5);
            
        }
        
    }
    label3.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.   Thick top and thin bottom
    
    [self.thickTopThinBottomImageButton setImage:nil    forState:UIControlStateNormal];
    [self.thickTopThinBottomImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.thickTopThinBottomImageButton .frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Thick top and thin bottom Lips", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.thickTopThinBottomImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.thickTopThinBottomImageButton.center;
    }
    else
    {
        label5.font = [UIFont boldSystemFontOfSize: 25];
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.thickTopThinBottomImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.thickTopThinBottomImageButton.center.x, self.thickTopThinBottomImageButton.center.y + 5);
        }
    }
    label5.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
    // 6.   Thick top and bottom
    
    [self.thickTopAndBottomImageButton setImage:nil    forState:UIControlStateNormal];
    [self.thickTopAndBottomImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label6 = nil;
    label6 = [[UILabel alloc] initWithFrame:self.thickTopAndBottomImageButton .frame];
    label6.backgroundColor = [UIColor clearColor];
    label6.text = NSLocalizedString(@"Thick top and bottom Lips", nil);;
    label6.font = [UIFont boldSystemFontOfSize: self.thickTopAndBottomImageButton.frame.size.height];
    label6.textColor = [UIColor redColor];
    label6.shadowColor = [UIColor lightGrayColor];
    label6.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label6 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label6.center = self.thickTopAndBottomImageButton.center;
    }
    else
    {
        label6.font = [UIFont boldSystemFontOfSize: 25];
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label6.center = self.thickTopAndBottomImageButton.center;
        }
        else
        {
            label6.center = CGPointMake(self.thickTopAndBottomImageButton.center.x, self.thickTopAndBottomImageButton.center.y + 5);
        }
    }
    
    label6.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label6];
    [label6 release];
    label6 = nil;

    
}




@end
