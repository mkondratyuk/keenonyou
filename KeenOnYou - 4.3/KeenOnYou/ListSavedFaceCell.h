//
//  ListSavedFaceCell.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListSavedFaceCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *avatarView;
@property (retain, nonatomic) IBOutlet UILabel *nameAvatarLabel;

@end
