//
//  AboutUsViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AboutUsViewController.h"
#import "AppDelegate.h"

@implementation AboutUsViewController

@synthesize scrollView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.scrollView.contentSize = CGSizeMake(0, 490);
    
    //  Localization of view
    
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }

}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setAboutUsImageButton:nil];
    [self setAboutUsTextView:nil];
    [self setWeLabel:nil];
    [self setTradeMarkLabel:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logoDidTap:(id)sender
{
    [Sound soundEffect:1];
    NSURL *url;
      
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if ([currentLanguage isEqualToString:@"ja"])
    {
        url = [NSURL URLWithString: @"http://www.keenonyou.com/index.php?lan=jp"];
    }
    else if ([currentLanguage isEqualToString:@"zh-Hans"])
    {
        url = [NSURL URLWithString: @"http://www.keenonyou.com/index.php?lan=cn"];
    }
    else
    {
        url = [NSURL URLWithString: @"http://www.keenonyou.com/index.php?lan=en"];
    }

     
     if (![[UIApplication sharedApplication] openURL:url])
     {
          DLog(@"%@%@",@"Failed to open url:",[url description]);
     }
}

- (void)dealloc
{
   
    [scrollView release];
    [_aboutUsImageButton release];
    [_aboutUsTextView release];
    [_weLabel release];
    [_tradeMarkLabel release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. About Us
    
    //  make image of button  nil (invisible)
    [self.aboutUsImageButton setImage:nil  forState:UIControlStateNormal];
    [self.aboutUsImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.aboutUsImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"About Us", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.aboutUsImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.aboutUsImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    CGRect frame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        frame = CGRectMake(91, 946, 123, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
             frame = CGRectMake(60, 507, 82, 31);
        }
        else
        {
             frame = CGRectMake(60, 417, 82, 31);
        }
    }

    
    UILabel *label4 = [[UILabel alloc] initWithFrame:frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    //label4.center = frame.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}


@end
