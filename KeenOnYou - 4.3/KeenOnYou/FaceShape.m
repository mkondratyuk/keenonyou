//
//  FaceShape.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FaceShape.h"
#import "ReadFace.h"
#import "LipsViewController.h"


@implementation FaceShape // word ViewController had lost :)


@synthesize currentReadFace;

@synthesize LongAndThinButton;
@synthesize averageButton;
@synthesize wideButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"]) {
        [self.LongAndThinButton setImage:[UIImage imageNamed:@"FaceShape_Male_LongAndThin.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"FaceShape_Male_Average.png"] forState:UIControlStateNormal]; 
        [self.wideButton setImage:[UIImage imageNamed:@"FaceShape_Male_Wide.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"]) {
        [self.LongAndThinButton setImage:[UIImage imageNamed:@"FaceShape_Female_LongAndThin.png"] forState:UIControlStateNormal] ;
        [self.averageButton setImage:[UIImage imageNamed:@"FaceShape_Female_Average.png"] forState:UIControlStateNormal] ; 
        [self.wideButton setImage:[UIImage imageNamed:@"FaceShape_Female_Wide.png"] forState:UIControlStateNormal] ;        
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
        
    }

    
}

- (void)viewDidUnload
{
    [self setLongAndThinButton:nil];
    [self setAverageButton:nil];
    [self setWideButton:nil];
    [self setFaceShapeImageButton:nil];
   
    [self setLongAndThinImageButton:nil];
    [self setAverageImageButton:nil];
    [self setWideImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark work with actions

- (IBAction)longAndThinDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.faceShape = @"LongAndThin";
    
    //LipsViewController *lvc= 
    
    //  block for load variouse
    
    LipsViewController *lvc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lvc = [[LipsViewController alloc] initWithNibName:@"LipsView-iPad"
                                           bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    lvc = [[LipsViewController alloc] initWithNibName: @"LipsView-568h"
                                                               bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    lvc= [[LipsViewController alloc]  initWithNibName:@"LipsViewController"
                                                               bundle:nil];
                }
            }
            else
            {
                //iphone screen
                lvc= [[LipsViewController alloc]  initWithNibName:@"LipsViewController"
                                                           bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    //  end block

    lvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:lvc animated:YES];
    [lvc release];    
    
}

- (IBAction)averageDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.faceShape = @"Average";
    
    //  block for load variouse
    
    LipsViewController *lvc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lvc = [[LipsViewController alloc] initWithNibName:@"LipsView-iPad"
                                                   bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    lvc = [[LipsViewController alloc] initWithNibName: @"LipsView-568h"
                                                               bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    lvc= [[LipsViewController alloc]  initWithNibName:@"LipsViewController"
                                                               bundle:nil];
                }
            }
            else
            {
                //iphone screen
                lvc= [[LipsViewController alloc]  initWithNibName:@"LipsViewController"
                                                           bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    //  end block
    
    lvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:lvc animated:YES];
    [lvc release];     
}

- (IBAction)wideFaceDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.faceShape = @"Wide";
    //  block for load variouse
    
    LipsViewController *lvc = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        lvc = [[LipsViewController alloc] initWithNibName:@"LipsView-iPad"
                                                   bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    lvc = [[LipsViewController alloc] initWithNibName: @"LipsView-568h"
                                                               bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    lvc= [[LipsViewController alloc]  initWithNibName:@"LipsViewController"
                                                               bundle:nil];
                }
            }
            else
            {
                //iphone screen
                lvc= [[LipsViewController alloc]  initWithNibName:@"LipsViewController"
                                                           bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    
    //  end block

    lvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:lvc animated:YES];
    [lvc release]; 
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark memory management

- (void)dealloc
{
    [currentReadFace release];
    [LongAndThinButton release];
    [averageButton release];
    [wideButton release];
    [_faceShapeImageButton release];
    [_longAndThinImageButton release];
    [_averageImageButton release];
    [_wideImageButton release];
    [_backImageButton release];
    
    [super dealloc];
}

#pragma mark
#pragma mark customizing localiziable button or labels


- (void) localizingThroughAddingLabels
{
    // 1. Face Shape
    
    //  make image of button  nil (invisible)
    [self.faceShapeImageButton setImage:nil  forState:UIControlStateNormal];
    [self.faceShapeImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.faceShapeImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Face Shape", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.faceShapeImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.faceShapeImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Long and Thin
    
    [self.longAndThinImageButton  setImage:nil forState:UIControlStateNormal];
    [self.longAndThinImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.longAndThinImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Long and Thin Face Shape", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.longAndThinImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.longAndThinImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.longAndThinImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.longAndThinImageButton.center.x, self.longAndThinImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Average
    
    [self.averageImageButton setImage:nil    forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Average Face Shape", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.averageImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.averageImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.averageImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.   Wide
    
    [self.wideImageButton setImage:nil    forState:UIControlStateNormal];
    [self.wideImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.wideImageButton .frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Wide Face Shape", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.wideImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.wideImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.wideImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.wideImageButton.center.x, self.wideImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}


@end
