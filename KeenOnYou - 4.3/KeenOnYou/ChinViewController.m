//
//  ChinViewController.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ChinViewController.h"
#import "ReadFace.h"
#import "MouthCornersViewController.h"

@implementation ChinViewController

@synthesize currentReadFace;

@synthesize smallButton;
@synthesize averageButton;
@synthesize wideButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"]) {
        [self.smallButton setImage:[UIImage imageNamed:@"Chin_Male_Small.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"Chin_Male_Average.png"] forState:UIControlStateNormal]; 
        [self.wideButton setImage:[UIImage imageNamed:@"Chin_Male_Wide.png"] forState:UIControlStateNormal];        
    }
    
    if ([self.currentReadFace.gender isEqualToString:@"Femail"]) {
        [self.smallButton setImage:[UIImage imageNamed:@"Chin_Female_Small.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"Chin_Female_Average.png"] forState:UIControlStateNormal]; 
        [self.wideButton setImage:[UIImage imageNamed:@"Chin_Female_Wide.png"] forState:UIControlStateNormal];         
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }

}

- (void)viewDidUnload
{
    [self setSmallButton:nil];
    [self setAverageButton:nil];
    [self setWideButton:nil];
    [self setChinImageButton:nil];
    [self setSmallImageButton:nil];
    [self setAverageImageButton:nil];
    [self setWideImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark memory management


- (void)dealloc
{
    [smallButton release];
    [averageButton release];
    [wideButton release];
    [_chinImageButton release];
    [_smallImageButton release];
    [_averageImageButton release];
    [_wideImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions

-(MouthCornersViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    MouthCornersViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[MouthCornersViewController alloc] initWithNibName:@"MouthCornersView-iPad"
                                                          bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[MouthCornersViewController alloc] initWithNibName: @"MouthCornersView-568h"
                                                                  bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[MouthCornersViewController alloc] initWithNibName:@"MouthCornersViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[MouthCornersViewController alloc] initWithNibName:@"MouthCornersViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
}

- (IBAction)smallButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.chin = @"Small";

    MouthCornersViewController *mcvc= [self newTrueView];
        
    mcvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:mcvc animated:YES];
    [mcvc release];
}

- (IBAction)averageButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.chin = @"Average";
    MouthCornersViewController *mcvc= [self newTrueView];
    mcvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:mcvc animated:YES];
    [mcvc release];
}

- (IBAction)wideButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.chin = @"Wide";
    MouthCornersViewController *mcvc= [self newTrueView];
    mcvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:mcvc animated:YES];
    [mcvc release];
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Chin
    
    //  make image of button  nil (invisible)
    [self.chinImageButton setImage:nil  forState:UIControlStateNormal];
    [self.chinImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.chinImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Chin", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.chinImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.chinImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Wide Chin
    
    [self.wideImageButton  setImage:nil forState:UIControlStateNormal];
    [self.wideImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.wideImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Wide Chin", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.wideImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.wideImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.wideImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.wideImageButton.center.x, self.wideImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Average Chin
    [self.averageImageButton setImage:nil    forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Average Chin", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.averageImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.averageImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.averageImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.  Small Chin
    
    [self.smallImageButton setImage:nil    forState:UIControlStateNormal];
    [self.smallImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.smallImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Small Chin", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.smallImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.smallImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.smallImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.smallImageButton.center.x, self.smallImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}


@end
