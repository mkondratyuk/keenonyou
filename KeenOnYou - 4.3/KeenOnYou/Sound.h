//
//  Sound.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 2/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface Sound : NSObject {
    
}

+ (void) soundEffect:(int)soundNumber;

- (void) playSystemSoundFromMainBundleWithFileName: (NSString *) name
                               andExtension: (NSString *) ext;

void SoundFinished (SystemSoundID snd, void* context);

@end