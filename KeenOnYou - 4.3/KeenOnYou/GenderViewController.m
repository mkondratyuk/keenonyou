//
//  GenderViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GenderViewController.h"
#import "ReadFace.h"
#import "TopOfHeadViewController.h"


@implementation GenderViewController

@synthesize loadFromMenuViewController;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //NSLog(@"WE ARE IN GenderViewController");

    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
        
    }

    
}

- (void)viewDidUnload
{
    [self setMailTextButton:nil];
    [self setFemaleTextButton:nil];
    [self setBackButton:nil];
    [self setGenderTextButton:nil];
    [self setGenderTextButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)mailButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    
    TopOfHeadViewController *tvc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        tvc = [[TopOfHeadViewController alloc] initWithNibName:@"TopOfHeadView-iPad"
                                                        bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    tvc = [[TopOfHeadViewController alloc] initWithNibName: @"TopOfHeadView-568h"
                                                                    bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    tvc = [[TopOfHeadViewController alloc] initWithNibName: @"TopOfHeadViewController"
                                                                    bundle: nil];
                }
            }
            else
            {
                //iphone screen
                tvc = [[TopOfHeadViewController alloc] initWithNibName: @"TopOfHeadViewController"
                                                                bundle: nil];
            }
            
            
        }
        else
        {
            DLog(@" ");
        }
    }

    ReadFace *currentReadFace = [[ReadFace alloc] init];
    tvc.currentReadFace = currentReadFace;
    [currentReadFace release];
    tvc.currentReadFace.gender = @"Mail";
    [self.navigationController pushViewController:tvc animated:YES];
    [tvc release]; 
}

- (IBAction)femailButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    
    TopOfHeadViewController *tvc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        tvc = [[TopOfHeadViewController alloc] initWithNibName:@"TopOfHeadView-iPad"
                                                    bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    tvc = [[TopOfHeadViewController alloc] initWithNibName: @"TopOfHeadView-568h"
                                                                    bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    tvc = [[TopOfHeadViewController alloc] initWithNibName: @"TopOfHeadViewController"
                                                                    bundle: nil];
                }
            }
            else
            {
                //iphone screen
                tvc = [[TopOfHeadViewController alloc] initWithNibName: @"TopOfHeadViewController"
                                                                bundle: nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    ReadFace *ivarReadFace = [[ReadFace alloc] init];
    tvc.currentReadFace = ivarReadFace;
    [ivarReadFace release];
    [tvc.currentReadFace setGender: @"Femail"];
    [self.navigationController pushViewController:tvc animated:YES];
    [tvc release]; 
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark memory management

- (void)dealloc 
{
    [_mailTextButton release];
    [_femaleTextButton release];
    [_backButton release];
    [_genderTextButton release];
    [super dealloc];
}

#pragma mark localization methods

- (void) localizingThroughButtons
{
    
    //  make image of button  nil (invisible)
    [self.mailTextButton setImage:nil
                         forState:UIControlStateNormal];
    NSString * title = nil;
    //  add to button text of title
    title = NSLocalizedString(@"Male", nil);
    [self.mailTextButton setTitle:title
                         forState:UIControlStateNormal];
    [self.mailTextButton setTitleColor:[UIColor redColor]
                              forState:UIControlStateNormal];
    [self.mailTextButton setTitleShadowColor:[UIColor blackColor]
                                    forState:UIControlStateNormal];
    
    //  make image of button  nil (invisible)
    [self.femaleTextButton setImage:nil
                           forState:UIControlStateNormal];
    
    //  add to button text of title
    title = NSLocalizedString(@"Female", nil);
    [self.femaleTextButton setTitle:title
                           forState:UIControlStateNormal];
    [self.femaleTextButton setTitleColor:[UIColor redColor]
                                forState:UIControlStateNormal];
    [self.femaleTextButton setTitleShadowColor:[UIColor blackColor]
                                      forState:UIControlStateNormal];
    
    
    //  make image of button  nil (invisible)
    [self.genderTextButton setImage:nil
                           forState:UIControlStateNormal];
    
    //  add to button text of title
    title = NSLocalizedString(@"Gender", nil);
    [self.genderTextButton setTitle:title
                           forState:UIControlStateNormal];
    [self.genderTextButton setTitleColor:[UIColor redColor]
                                forState:UIControlStateNormal];
    [self.genderTextButton setTitleShadowColor:[UIColor blackColor]
                                      forState:UIControlStateNormal];
    self.genderTextButton.transform = CGAffineTransformMakeScale(2,2);
    
    
    [self.backButton setImage:nil
                     forState:UIControlStateNormal];
    
    //  add to button text of title
    title = NSLocalizedString(@"Back", nil);
    [self.backButton setTitle:title
                     forState:UIControlStateNormal];
    [self.backButton setTitleColor:[UIColor redColor]
                          forState:UIControlStateNormal];
    [self.backButton setTitleShadowColor:[UIColor blackColor]
                                forState:UIControlStateNormal];
    self.backButton.transform = CGAffineTransformMakeScale(2,2);
    
    if([[UIScreen mainScreen] bounds].size.height == 480)
    {
        CGRect buferFrame = self.mailTextButton.frame;
        buferFrame.size.height = buferFrame.size.height + 30;
        self.mailTextButton.frame = buferFrame;
        
        buferFrame = self.femaleTextButton.frame;
        buferFrame.size.height = buferFrame.size.height + 30;
        self.femaleTextButton.frame = buferFrame;
    }

}


- (void) localizingThroughAddingLabels
{
    // 1.    Gender
    
    //  make image of button  nil (invisible)
    [self.genderTextButton setImage:nil  forState:UIControlStateNormal];
    [self.genderTextButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.genderTextButton.frame];
    //label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Gender", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.genderTextButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.genderTextButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Male
    //  make image of button  nil (invisible)
    [self.mailTextButton setImage:nil
                         forState:UIControlStateNormal];
    [self.mailTextButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.genderTextButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Male", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.mailTextButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.mailTextButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.mailTextButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.mailTextButton.center.x, self.mailTextButton.center.y + 20);
        }
        
    }

    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3. Female
    
    [self.femaleTextButton setImage:nil forState:UIControlStateNormal];
    [self.femaleTextButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.femaleTextButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Female", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.femaleTextButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.femaleTextButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.femaleTextButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.femaleTextButton.center.x, self.femaleTextButton.center.y + 20);
        }
        
    }
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 4. Back
    
    [self.backButton setImage:nil forState:UIControlStateNormal];
    [self.backButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;

    
}



@end
