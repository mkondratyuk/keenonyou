//
//  EyeSpacingViewController.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EyeSpacingViewController.h"
#import "ReadFace.h"
#import "CheckbonesViewController.h"

@implementation EyeSpacingViewController

@synthesize currentReadFace;

@synthesize wideButton;
@synthesize averageButton;
@synthesize closeButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        [self.wideButton setImage:[UIImage imageNamed:@"EyeSpacing_Male_Wide.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"EyeSpacing_Male_Average.png"] forState:UIControlStateNormal]; 
        [self.closeButton setImage:[UIImage imageNamed:@"EyeSpacing_Male_Close.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"])
    {
        [self.wideButton setImage:[UIImage imageNamed:@"EyeSpacing_Female_Wide.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"EyeSpacing_Female_Average.png"] forState:UIControlStateNormal]; 
        [self.closeButton setImage:[UIImage imageNamed:@"EyeSpacing_Female_Close.png"] forState:UIControlStateNormal];        
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }
}

- (void)viewDidUnload
{
    [self setWideButton:nil];
    [self setAverageButton:nil];
    [self setCloseButton:nil];

    [self setEyeSpacingImageButton:nil];
    [self setWideImageButton:nil];
    [self setAverageImageButton:nil];
    [self setCloseImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


#pragma mark memory management

- (void)dealloc {
    [wideButton release];
    [averageButton release];
    [closeButton release];
  
    [_eyeSpacingImageButton release];
    [_wideImageButton release];
    [_averageImageButton release];
    [_closeImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions

-(CheckbonesViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    CheckbonesViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[CheckbonesViewController alloc] initWithNibName:@"CheckbonesView-iPad"
                                                             bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[CheckbonesViewController alloc] initWithNibName: @"CheckbonesView-568h"
                                                                         bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[CheckbonesViewController alloc] initWithNibName:@"CheckbonesViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[CheckbonesViewController alloc] initWithNibName:@"CheckbonesViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
    
}

- (IBAction)wideButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyeSpacing = @"Wide";
    
    CheckbonesViewController *cvc= [self newTrueView];
    
    cvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];
}

- (IBAction)averageButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyeSpacing = @"Average";
    
    CheckbonesViewController *cvc= [self newTrueView];
    
    cvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];
}   


- (IBAction)closeButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyeSpacing = @"Close";
    
    CheckbonesViewController *cvc= [self newTrueView];
    
    cvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Eye Spacing
    
    //  make image of button  nil (invisible)
    [self.eyeSpacingImageButton setImage:nil  forState:UIControlStateNormal];
    [self.eyeSpacingImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.eyeSpacingImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Eye Spacing", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.eyeSpacingImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.eyeSpacingImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Wide Eye Spacing
    
    [self.wideImageButton  setImage:nil forState:UIControlStateNormal];
    [self.wideImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.wideImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Wide Eye Spacing", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.wideImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.wideImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.wideImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.wideImageButton.center.x, self.wideImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Average Eye Spacing
    
    [self.averageImageButton setImage:nil    forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Average Eye Spacing", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.averageImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.averageImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.averageImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.  Close Eye Spacing
    [self.closeImageButton setImage:nil    forState:UIControlStateNormal];
    [self.closeImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.closeImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Close Eye Spacing", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.closeImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.closeImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.closeImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.closeImageButton.center.x, self.closeImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}



@end
