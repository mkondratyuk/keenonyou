//
//  ListSavedFacesTableViewController.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListSavedFacesTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSMutableArray *listOfSavedFaces;
@property (nonatomic, assign) BOOL didPassedFromFaceReading;

//  for localizing ...
@property (retain, nonatomic) IBOutlet UIButton *faceHistoryImageButton;
@property (retain, nonatomic) IBOutlet UIButton *menuImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;


- (IBAction)backButtonDidTap:(id)sender;
- (IBAction)menuButtonDidTap:(id)sender;

@end
