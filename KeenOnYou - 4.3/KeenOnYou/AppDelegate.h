//
//  AppDelegate.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#define KeenOnYouNotificationSettingsDidUpdate @"KeenOnYouNotificationSettingsDidUpdate"


#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate, AVAudioSessionDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) AVAudioPlayer *player;

@end
