//
//  Choise.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Choise.h"

@implementation Choise

@synthesize art, desc, ifYou;

#pragma mark - designated initializer
- (id) initWithArt: (NSString *) a
       desc: (NSMutableArray *) d
ifYou: (NSString *) try
{
    // call the superclass's designed initializer
    self = [super init];
    // did the superclass's designed initializer succed?
    if (self)
    {
        // give the instance variables initial values
        [self setArt:a];
        [self setDesc:[NSMutableArray arrayWithCapacity:0]];
        [desc retain];
        [self setIfYou:try];
    }
    // return the address of newly initialized object
    return self;
}

#pragma mark - memory management 

- (void) dealloc
{
    [ art release];
    [desc release];
    [ifYou release];
    [super dealloc];
}

@end
