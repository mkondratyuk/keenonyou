//
//  FaceReadingViewController.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FaceReadingViewController.h"
#import "ReadFace.h"
#import "ChoisesStore.h"
#import "RowTextViewController.h"
#import "FileHelpers.h"
#import "DetailViewController.h"
#import "QuartzCore/QuartzCore.h"

@interface FaceReadingViewController ()

{
    NSMutableArray *namedFeatureDesc;
    NSMutableArray *featureDesc;
    
    NSMutableArray *namedFeatureIfYou;
    NSMutableArray *featureIfYou;
    
    NSUInteger indexInListOfSavedFaces;
    
    BOOL isCustomItemDidSelected;
    NSIndexPath *selectedIndexPath;
    CGPoint savedOffset;
   
}

- (void) drawAvatar;
- (BOOL) inputNameDidExist: (NSString *) fileNamePNG;
- (void) saveCurrentAvatarImageInFilePNG;
- (NSData *)takeThumbnailDataFromImage:(UIImage *)image;
- (void) writeToPropertyListDataOfAvatar;

- (NSUInteger) countNumberDescForAvatar;
- (NSUInteger) countIfYouForAvatar;

@end

@implementation FaceReadingViewController
@synthesize viewWithDisclaimmerText;

@synthesize scrollView;
@synthesize nameOfAvatarTextField;
@synthesize backgraundImageViewForAvatar;

@synthesize tableView;

@synthesize currentReadFace;
@synthesize arrayDescForAvatar, arrayIfYouForAvatar;
@synthesize numberItemsInArrayDescForAvatar, numberItemsInArrayIfYouForAvatar;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    DLog(@" " );
    // Release any cached data, images, etc that aren't in use.
    
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    isCustomItemDidSelected = NO;
    
    //self.scrollView.contentSize = CGSizeMake(0, 800);    
    // 
    DLog(@"OUR currentReadFace is %@", currentReadFace);
    
    arrayDescForAvatar = [NSMutableArray arrayWithCapacity:0];
    arrayIfYouForAvatar = [NSMutableArray arrayWithCapacity:0];
    [arrayDescForAvatar retain];
    [arrayIfYouForAvatar retain];
    
    namedFeatureDesc = [NSMutableArray arrayWithCapacity:0];
    featureDesc = [NSMutableArray arrayWithCapacity:0];
    
    namedFeatureIfYou = [NSMutableArray arrayWithCapacity:0];
    featureIfYou = [NSMutableArray arrayWithCapacity:0];
    
    
    [namedFeatureIfYou retain];
    [namedFeatureDesc retain];
    
    [featureIfYou retain];
    [featureDesc retain];
    
    //  forming arrayDescForAvatar
    numberItemsInArrayDescForAvatar = [self countNumberDescForAvatar];
    //  forming arrayIfYouForAvatar
    numberItemsInArrayIfYouForAvatar = [self countIfYouForAvatar];

    
    
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self
               selector:@selector(handleKeyboardWillShow:)
                   name:UIKeyboardWillShowNotification
                 object:nil];
    
    [center addObserver:self
               selector:@selector(handleKeyboardWillHide:)
                   name:UIKeyboardWillHideNotification
                 object:nil];
    
    [Sound soundEffect:2];
    
    // make rounded border line for view with disclaimmer text
    /*
    [self.viewWithDisclaimmerText.layer setCornerRadius:10.0f];
    UIColor *customBorderColor = [[UIColor alloc] initWithRed: 124.0f/255.0f
                                                        green: 157.0f/255.0f
                                                         blue: 18.0f/255.0f
                                                        alpha: 1.0 ];
    [self.viewWithDisclaimmerText.layer setBorderColor:[customBorderColor CGColor]];
    [customBorderColor release];
    [self.viewWithDisclaimmerText.layer setBorderWidth:1.0f];
     */

    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }
    
    self.nameOfAvatarTextField.placeholder = NSLocalizedString(@"Enter Name", nil);
}

- (void) viewWillAppear:(BOOL)animated
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        self.tableView.rowHeight = 66.0;
        double height = ([arrayDescForAvatar count] + [arrayIfYouForAvatar count] ) * 66.0 + 90.0 + 61;
        
        //tableView.contentSize = CGSizeMake(0, height);
        tableView.frame = CGRectMake(9.0, 950.0, 750.0, height + 20.0);
        viewWithDisclaimmerText.frame = CGRectMake(0, 950 + height + 30, 768, 90);
        scrollView.contentSize = CGSizeMake(0, 950.0 + height + 60.0 + 40.0 + 50.0 + 30.0 );

    }
    else
    {       //  iPhone !!!
        double height = ([arrayDescForAvatar count] + [arrayIfYouForAvatar count] ) * 44.0 + 90.0 + 30;
    
        //tableView.contentSize = CGSizeMake(0, height);
        tableView.frame = CGRectMake(10.0, 370.0 + 10.0, 300.0, height);
        viewWithDisclaimmerText.frame = CGRectMake(0, 370 + height + 10, 320, 100.0);
        scrollView.contentSize = CGSizeMake(0, 380.0 + height + 60.0 + 40.0 + 30.0 );
    }
  
   
#ifndef __IPHONE_6_0
    NSDictionary *underlineAttribute = @{ NSUnderlineStyleAttributeName: @1 };
    self.howToReadFacesLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"How to Read Faces", nil)
                                                                              attributes:underlineAttribute];
#else
    
#endif
   
    [tableView reloadData];
    
    [self drawAvatar];

}

- (void)viewDidUnload
{
    [self setScrollView:nil];
    [self setNameOfAvatarTextField:nil];

    [self setTableView:nil];
    [self setBackgraundImageViewForAvatar:nil];
   
    [self setViewWithDisclaimmerText:nil];
    [self setFaceReadingImageButton:nil];
    [self setSaveImageButton:nil];
    [self setMenuImageButton:nil];
    [self setBackImageButton:nil];
    [self setDisclaimerLabel:nil];
    [self setFirstLineLabel:nil];
    [self setSecondLineLabel:nil];
    [self setThirdLineLabel:nil];
    [self setFourthLineLabel:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark internal methods

- (NSUInteger) countNumberDescForAvatar 
{
    NSArray *arrayOfChoises = [[ChoisesStore defaultStore] allChoices];
    
    NSInteger numberDescs = 0;
    int i = 0;
    for (NSDictionary *firstLevelDictionary in arrayOfChoises)
    {
         NSDictionary *secondLevelDictionary = nil;
        if (i == 0)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.topOfHead];
        }
        if (i == 1)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.faceShape];
        }
        if (i == 2)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.lips];
        }
        if (i == 3)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.eyebrowShape];
        }
        if (i == 4)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.eyebrowHeight];
        }
        if (i == 5)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.eyeSpacing];
        }
        if (i == 6)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.cheeckbones];
        }
        if (i == 7)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.nostrils];
        }
        if (i == 8)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.noseUnderside];
        }
        if (i == 9)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.chin];
        }
        if (i == 10)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.mouthCorners];
        }
            
        NSArray *arrayOfDesc = [secondLevelDictionary objectForKey:@"Desc"];
        for (NSString *desc in arrayOfDesc)
        {
            if (![desc isEqual:[NSNull null]])
            {
                numberDescs = numberDescs + 1;
                [arrayDescForAvatar addObject:desc];
            }
        }
        i = i + 1;
    }
    
    DLog(@"NUMBER =DESCRIPTIONS= = %d  and array of desc = %@", numberDescs, arrayDescForAvatar);
            
    return numberDescs;
}

- (NSUInteger) countIfYouForAvatar
{
    NSArray *arrayOfChoises = [[ChoisesStore defaultStore] allChoices];
    
    NSInteger numberIfYou = 0;
    int i = 0;
        
    for (NSDictionary *firstLevelDictionary in arrayOfChoises)
    {
        NSDictionary *secondLevelDictionary = nil;
        if (i == 0)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.topOfHead];
        }
        if (i == 1)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.faceShape];
        }
        if (i == 2)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.lips];
        }
        if (i == 3)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.eyebrowShape];
        }
        if (i == 4)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.eyebrowHeight];
        }
        if (i == 5)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.eyeSpacing];
        }
        if (i == 6)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.cheeckbones];
        }
        if (i == 7)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.nostrils];
        }
        if (i == 8)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.noseUnderside];
        }
        if (i == 9)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.chin];
        }
        if (i == 10)
        {
            secondLevelDictionary = [firstLevelDictionary objectForKey:self.currentReadFace.mouthCorners];
        }
        NSString *stringOfIfYou = [secondLevelDictionary objectForKey:@"IfYou"];
        if ([stringOfIfYou length] > 0)
        {
            numberIfYou = numberIfYou + 1;
            [arrayIfYouForAvatar addObject:stringOfIfYou];
        }
        i = i + 1;
    }
    DLog(@"NUMBER  =IF YOU ?= = %d and array of ifYou = %@", numberIfYou, arrayIfYouForAvatar);
    
    return numberIfYou;

}



- (void) drawAvatar
{
    NSArray *arrayOfChoises = [[ChoisesStore defaultStore] allChoices];
    
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        // MAIL PART
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.backgraundImageViewForAvatar setImage:[UIImage imageNamed:@"Background_Face_Male-750x750.png"]];
        }
        else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            [self.backgraundImageViewForAvatar setImage:[UIImage imageNamed:@"Background_Face_Male-260x260.png"]];
        }
        else
        {
            DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
        }
        
        // Lips set
        NSDictionary *lips = [arrayOfChoises objectAtIndex:2];
        NSDictionary *varLips = [lips objectForKey:self.currentReadFace.lips];
        
        NSString * nameFileLips = [varLips objectForKey:@"Art"];
        NSString *nameFileLipsMail = nil;
        if ([nameFileLips length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileLips isEqualToString:@"Lips_ThinTop-ThickBottom.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileLipsMail = @"Lips_ThinTop-ThickBottom_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileLipsMail = @"Lips_ThinTop-ThickBottom_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileLips isEqualToString:@"Lips_ThickTopAndThinBottom.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileLipsMail = @"Lips_ThickTopAndThinBottom_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileLipsMail = @"Lips_ThickTopAndThinBottom_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileLips isEqualToString:@"Lips_ThickTopAndBottom.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileLipsMail = @"Lips_ThickTopAndBottom_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileLipsMail = @"Lips_ThickTopAndBottom_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR of name of image of lips  ");
            }
            
            // add  lips view
            UIImageView * lipsImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
             DLog(@"name loading in UIImageView file for Lips = %@", nameFileLipsMail);
            [lipsImageView setImage: [UIImage imageNamed:nameFileLipsMail]];
            [self.backgraundImageViewForAvatar addSubview:lipsImageView];
            [lipsImageView release];
        }
       
        // Eyebrow Shapes set
        NSDictionary *eyebrowShapes = [arrayOfChoises objectAtIndex:3];
        NSDictionary *varEyebrowShapes = [eyebrowShapes objectForKey:self.currentReadFace.eyebrowShape];
        //
        NSString * nameFileEyebrowShapes = [varEyebrowShapes objectForKey:@"Art"];
        NSString *nameFileEyebrowShapesMale = nil;
        if ([nameFileEyebrowShapes length] > 1 )
        {
            // loading appropriate image
            if ([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Average.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Average_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Average_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Thick.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Thick_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Thick_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Straight.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Straight_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Straight_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_SharpPoint.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_SharpPoint_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_SharpPoint_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Semi-Circle.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Semi-Circle_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_Semi-Circle_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_HighArc.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_HighArc_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapesMale = @"Eyebrows_HighArc_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR  name of image file of eybrows");
            }
            
            // add 
            UIImageView * eyebrowShapesImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
             DLog(@"name loading in UIImageView file for EyebrowShapes = %@", nameFileEyebrowShapesMale);
            [eyebrowShapesImageView setImage: [UIImage imageNamed:nameFileEyebrowShapesMale]];
            [self.backgraundImageViewForAvatar addSubview:eyebrowShapesImageView];
            [eyebrowShapesImageView release];
        }
        
        // chin set
        NSDictionary *chin = [arrayOfChoises objectAtIndex:9];
        NSDictionary *varChin = [chin objectForKey:self.currentReadFace.chin];
        NSString * nameFileChin = [varChin objectForKey:@"Art"];
        NSString *nameFileChinMale = nil;
        if ([nameFileChin length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileChin isEqualToString:@"Chin_Small.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileChinMale = @"Chin_Small_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileChinMale = @"Chin_Small_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileChin isEqualToString:@"Chin_Wide.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileChinMale = @"Male_WideChin-750x750.png";
                    //nameFileChinMale = @"Chin_Wide_Male_New-750x750.png";

                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileChinMale = @"Male_WideChin-260x260.png";
                    //nameFileChinMale = @"Chin_Wide_Male_New-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR of name of image of lips  ");
            }
            
            // add
            UIImageView * chinImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
            DLog(@"name loading in UIImageView file for Chin = %@", nameFileChinMale);
            [chinImageView setImage: [UIImage imageNamed:nameFileChinMale]];
            /*
            if([nameFileChin isEqualToString:@"Chin_Wide.png"])
            {
                CGRect frame;
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    frame = chinImageView.frame;
                    frame.origin = CGPointMake(frame.origin.x - 23.0, frame.origin.y + 40.0);
                    chinImageView.frame = frame;
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    frame = chinImageView.frame;
                    frame.origin = CGPointMake(frame.origin.x - 8.0, frame.origin.y + 14.0);
                    chinImageView.frame = frame;
                    ;
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            */
            
            [self.backgraundImageViewForAvatar addSubview:chinImageView];
            [chinImageView release];
        }      
        
        // cheekbones set
        NSDictionary *cheekbones = [arrayOfChoises objectAtIndex:6];
        NSDictionary *var = [cheekbones objectForKey:self.currentReadFace.cheeckbones];
        
        NSString * nameFileCheekbones = [var objectForKey:@"Art"];
        NSString *nameFileMaleCheekbones = nil;
        if ([nameFileCheekbones length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileCheekbones isEqualToString:@"Cheekbones_Soft.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileMaleCheekbones = @"Cheekbones_Soft_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileMaleCheekbones = @"Cheekbones_Soft_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileCheekbones isEqualToString:@"Cheekbones_HighDefined.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileMaleCheekbones = @"Cheekbones_HighDefined_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileMaleCheekbones = @"Cheekbones_HighDefined_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR");
            }
            // add 
            UIImageView * cheeckbonesImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
            DLog(@"name loading in UIImageView file for Cheekbones = %@", nameFileMaleCheekbones);
            [cheeckbonesImageView setImage: [UIImage imageNamed:nameFileMaleCheekbones]];
            [self.backgraundImageViewForAvatar addSubview:cheeckbonesImageView];
            [cheeckbonesImageView release];
        }
        
        // Nostril set
        NSDictionary *nostrils = [arrayOfChoises objectAtIndex:7];
        NSDictionary *varNostrils = [nostrils objectForKey:self.currentReadFace.nostrils];
        
        NSString * nameFileNostrils = [varNostrils objectForKey:@"Art"];
        NSString *nameFileNostrilsMale = nil;
        if ([nameFileNostrils length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileNostrils isEqualToString:@"Nostril_Average.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileNostrilsMale = @"Nostrils_Average_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileNostrilsMale = @"Nostrils_Average_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileNostrils isEqualToString:@"Nostril_Wide.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileNostrilsMale = @"Nostrils_Wide_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileNostrilsMale = @"Nostrils_Wide_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileNostrils isEqualToString:@"Nostril_Narrow.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileNostrilsMale = @"Nostrils_Narrow_Male-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileNostrilsMale = @"Nostrils_Narrow_Male-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR of name of image of lips  ");
            }
            
            // add 
            UIImageView * nostrilsImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
             DLog(@"name loading in UIImageView file for Nostril = %@", nameFileNostrilsMale);
            [nostrilsImageView setImage: [UIImage imageNamed:nameFileNostrilsMale]];
            [self.backgraundImageViewForAvatar addSubview:nostrilsImageView];
            [nostrilsImageView release];
        }
        
                
    }
    else
    {
        //  FEMAIL PART
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            [self.backgraundImageViewForAvatar setImage:[UIImage imageNamed:@"Female - Background_Face-750x750.png"]];
        }
        else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
        {
            [self.backgraundImageViewForAvatar setImage:[UIImage imageNamed:@"Female - Background_Face-260x260.png"]];
        }
        else
        {
            DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
        }
       
        // Lips set
        NSDictionary *lips = [arrayOfChoises objectAtIndex:2];
        NSDictionary *varLips = [lips objectForKey:self.currentReadFace.lips];
        NSString * nameFileLips = [varLips objectForKey:@"Art"];
        if ([nameFileLips length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileLips isEqualToString:@"Lips_ThinTop-ThickBottom.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileLips = @"Lips_ThinTop-ThickBottom-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileLips = @"Lips_ThinTop-ThickBottom-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileLips isEqualToString:@"Lips_ThickTopAndThinBottom.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileLips = @"Lips_ThickTopAndThinBottom-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileLips = @"Lips_ThickTopAndThinBottom-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileLips isEqualToString:@"Lips_ThickTopAndBottom.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileLips = @"Lips_ThickTopAndBottom-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileLips = @"Lips_ThickTopAndBottom-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR of name of image of lips  ");
            }
            
            // add 
            UIImageView * lipsImageView =  [[UIImageView alloc] initWithFrame:[self.backgraundImageViewForAvatar bounds]];
             DLog(@"name loading in UIImageView file for Lips = %@", nameFileLips);
            [lipsImageView setImage: [UIImage imageNamed:nameFileLips]];
            [self.backgraundImageViewForAvatar addSubview:lipsImageView];
            [lipsImageView release];
        }
        
        // Eyebrow Shapes set
        NSDictionary *eyebrowShapes = [arrayOfChoises objectAtIndex:3];
        NSDictionary *varEyebrowShapes = [eyebrowShapes objectForKey:self.currentReadFace.eyebrowShape];
        NSString * nameFileEyebrowShapes = [varEyebrowShapes objectForKey:@"Art"];
        if ([nameFileEyebrowShapes length] > 1 )
        {
            // loading appropriate image
            if ([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Average.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Average-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Average-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Thick.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Thick-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Thick-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Straight.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Straight-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Straight-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_SharpPoint.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapes = @"Eyebrows_SharpPoint-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapes = @"Eyebrows_SharpPoint-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_Semi-Circle.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Semi-Circle-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapes = @"Eyebrows_Semi-Circle-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileEyebrowShapes isEqualToString:@"Eyebrows_HighArc.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileEyebrowShapes = @"Eyebrows_HighArc-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileEyebrowShapes = @"Eyebrows_HighArc-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR  name of image file of eybrows");
            }

            // add
            UIImageView * eyebrowShapesImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
             DLog(@"name loading in UIImageView file for EyebrowShapes = %@", nameFileEyebrowShapes);
            [eyebrowShapesImageView setImage: [UIImage imageNamed:nameFileEyebrowShapes]];
            [self.backgraundImageViewForAvatar addSubview:eyebrowShapesImageView];
            [eyebrowShapesImageView release];
        }

        // cheekbones set
        NSDictionary *cheekbones = [arrayOfChoises objectAtIndex:6];
        NSDictionary *var = [cheekbones objectForKey:self.currentReadFace.cheeckbones];
        NSString * nameFileCheekbones = [var objectForKey:@"Art"];
        if ([nameFileCheekbones length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileCheekbones isEqualToString:@"Cheekbones_Soft.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileCheekbones = @"Cheekbones_Soft-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileCheekbones = @"Cheekbones_Soft-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
                
                
            }
            else if([nameFileCheekbones isEqualToString:@"Cheekbones_HighDefined.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileCheekbones = @"Cheekbones_HighDefined-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileCheekbones = @"Cheekbones_HighDefined-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR");
            }
            
            // add 
            UIImageView * cheeckbonesImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
            DLog(@"name loading in UIImageView file for Cheekbones = %@", nameFileCheekbones);
            [cheeckbonesImageView setImage: [UIImage imageNamed:nameFileCheekbones]];
            [self.backgraundImageViewForAvatar addSubview:cheeckbonesImageView];
            [cheeckbonesImageView release];
        }
        
        // Nostril set
        NSDictionary *nostrils = [arrayOfChoises objectAtIndex:7];
        NSDictionary *varNostrils = [nostrils objectForKey:self.currentReadFace.nostrils];
        NSString * nameFileNostrils = [varNostrils objectForKey:@"Art"];
        if ([nameFileNostrils length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileNostrils isEqualToString:@"Nostril_Average.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileNostrils = @"Nostril_Average-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileNostrils = @"Nostril_Average-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileNostrils isEqualToString:@"Nostril_Wide.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileNostrils = @"Nostril_Wide-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileNostrils = @"Nostril_Wide-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileNostrils isEqualToString:@"Nostril_Narrow.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileNostrils = @"Nostril_Narrow-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileNostrils = @"Nostril_Narrow-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR of name of image of lips  ");
            }
            
            // add 
            UIImageView * nostrilsImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
            DLog(@"name loading in UIImageView file for Nostrils = %@", nameFileNostrils);
            [nostrilsImageView setImage: [UIImage imageNamed:nameFileNostrils]];
            [self.backgraundImageViewForAvatar addSubview:nostrilsImageView];
            [nostrilsImageView release];
        }
        
        // chin set
        NSDictionary *chin = [arrayOfChoises objectAtIndex:9];
        NSDictionary *varChin = [chin objectForKey:self.currentReadFace.chin];
        NSString * nameFileChin = [varChin objectForKey:@"Art"];
        if ([nameFileChin length] > 1 )
        {
            // load needed(appropriate) image
            if ([nameFileChin isEqualToString:@"Chin_Small.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileChin = @"Chin_Small-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileChin = @"Chin_Small-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else if([nameFileChin isEqualToString:@"Chin_Wide.png"])
            {
                if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
                {
                    nameFileChin = @"Chin_Wide-750x750.png";
                }
                else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
                {
                    nameFileChin = @"Chin_Wide-260x260.png";
                }
                else
                {
                    DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
                }
            }
            else
            {
                DLog(@"IN drawAvatar ERROR of name of image of lips  ");
            }
            
            // add 
            UIImageView * chinImageView =  [[UIImageView alloc] initWithFrame:[backgraundImageViewForAvatar bounds]];
            DLog(@"name loading in UIImageView file for Chin = %@", nameFileChin);
            [chinImageView setImage: [UIImage imageNamed:nameFileChin]];
            [self.backgraundImageViewForAvatar addSubview:chinImageView];
            [chinImageView release];
        }
    }
    
}

#pragma mark internal methods for save avatar 

- (BOOL)checkNameValidate
{
    
	NSString* alertBody;
    
	if ([self.nameOfAvatarTextField.text length] == 0)
    {
		alertBody = NSLocalizedString(@"Please enter name", nil);
    }
    else if ([self inputNameDidExist: self.nameOfAvatarTextField.text])
    {
		alertBody = NSLocalizedString(@"Name already exists, please enter in more details.", nil);
    }
    else
    {
		return YES;
    }
    
	UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"" 
                                                    message:alertBody 
                                                   delegate:nil 
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
	[alert show];
	[alert release];
	return NO;
}

- (BOOL) inputNameDidExist: (NSString *) fileNameWithWillAddPNG 
{   
    NSFileManager *fm = [NSFileManager defaultManager];
    NSMutableString *pathname = [NSMutableString stringWithCapacity:0];
    [pathname setString:fileNameWithWillAddPNG];
    [pathname appendString:@".png"];
    DLog(@" NAME OF FILE WITH .png =  %@", pathInDocumentDirectory([pathname substringFromIndex:0]));
    return [fm fileExistsAtPath:pathInDocumentDirectory([pathname substringFromIndex:0])];
}

- (void) saveCurrentAvatarImageInFilePNG
{
    NSMutableString *pathname = [NSMutableString stringWithCapacity:0];
    [pathname setString:nameOfAvatarTextField.text];
    [pathname appendString:@".png"];
    DLog(@" NAME OF FILE WITH .png =  %@", pathInDocumentDirectory([pathname substringFromIndex:0]));
    
    //создание контекста...
    UIGraphicsBeginImageContext(backgraundImageViewForAvatar.frame.size);
    
    // вывод содержимого в этот контекст
    [self.backgraundImageViewForAvatar.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    // создание UIImage
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();  
      
    // cоздание NSData
    NSData *pngData = UIImagePNGRepresentation(image);
    
    //
    [pngData writeToFile:pathInDocumentDirectory([pathname substringFromIndex:0]) atomically:YES];
    // закрытие контекста
    UIGraphicsEndImageContext();
    
    // cоздание миниатюры для таблицы и сохранение ее в файле
    
    [pathname insertString:@"small" atIndex:0];
     DLog(@" NAME OF FILE thumbnail =  %@", pathInDocumentDirectory([pathname substringFromIndex:0]));
    // cоздание NSData
    NSData *pngDataThumbnail = [self takeThumbnailDataFromImage:image];
    
    //
    [pngDataThumbnail writeToFile:pathInDocumentDirectory([pathname substringFromIndex:0]) atomically:YES];
    
}

- (NSData *)takeThumbnailDataFromImage:(UIImage *)image
{
    CGSize origImageSize = [image size];
    CGRect newRect;
    newRect.origin = CGPointZero;
    newRect.size = CGSizeMake(40, 40);
    // How do we scale the image?
    float ratio = MAX(newRect.size.width/origImageSize.width,
                      newRect.size.height/origImageSize.height);
    // Create a bitmap image context
    UIGraphicsBeginImageContext(newRect.size);
    // Round the corners
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:newRect
                                                    cornerRadius:5.0];
    [path addClip];
    // Into what rectangle shall I composite the image?
    CGRect projectRect;
    projectRect.size.width = ratio * origImageSize.width;
    projectRect.size.height = ratio * origImageSize.height;
    projectRect.origin.x = (newRect.size.width - projectRect.size.width) / 2.0;
    projectRect.origin.y = (newRect.size.height - projectRect.size.height) / 2.0;
    // Draw the image on it
    [image drawInRect:projectRect];
    // Get the image from the image context, retain it as our thumbnail
    UIImage *small = UIGraphicsGetImageFromCurrentImageContext();
    
    // Get the image as a PNG data
    NSData *data = UIImagePNGRepresentation(small);
   
    // Cleanup image contex resources, we're done
    UIGraphicsEndImageContext();
    return data;
}

- (void) writeToPropertyListDataOfAvatar 
{
    NSDictionary *dataOfAvatar = [NSDictionary dictionaryWithObjectsAndKeys:arrayDescForAvatar,@"Desc", arrayIfYouForAvatar, @"IfYou", nameOfAvatarTextField.text, @"Name",nil];
    DLog(@"dataOfAvatar = %@", dataOfAvatar);
    
    NSString *fileNameListOfSavedFaces = pathInDocumentDirectory(@"listsavedfaces");
    NSMutableArray *listOfsavedFaces = [NSMutableArray arrayWithContentsOfFile:fileNameListOfSavedFaces];
    if (listOfsavedFaces == nil)
    {
        listOfsavedFaces =[NSMutableArray arrayWithCapacity:0];
    }
   
    [listOfsavedFaces addObject:dataOfAvatar];
    indexInListOfSavedFaces = [listOfsavedFaces count] - 1;

    if ([listOfsavedFaces writeToFile:fileNameListOfSavedFaces atomically:YES] == NO)
    {
        DLog(@"Save to file listsavedfaces failed!");
    }

}



#pragma mark memory management

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [arrayDescForAvatar release];
    [arrayIfYouForAvatar release];
    
    [namedFeatureDesc release];
    [featureDesc release];
    [namedFeatureIfYou release];
    [featureIfYou release];
    
    [scrollView release];
    [nameOfAvatarTextField release];
    [tableView release];
    [backgraundImageViewForAvatar release];
    
    [viewWithDisclaimmerText release];
    [_howToReadFacesLabel release];
    [_faceReadingImageButton release];
    [_saveImageButton release];
    [_menuImageButton release];
    [_backImageButton release];
    [_disclaimerLabel release];
    [_firstLineLabel release];
    [_secondLineLabel release];
    [_thirdLineLabel release];
    [_fourthLineLabel release];
    [super dealloc];
}


#pragma mark work with action


- (IBAction)saveButtonDidTap:(id)sender 
{
    [Sound soundEffect:1];
    [nameOfAvatarTextField resignFirstResponder];
    
    // test input name of avatar
    if ([self checkNameValidate])
    {
        // save avatar UIImage
        [self saveCurrentAvatarImageInFilePNG];
        // save data in XML property list
        [self writeToPropertyListDataOfAvatar];
        
        // 
        
        DetailViewController *detailViewController = nil;
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailView-iPad" bundle:nil];
            
        }
        else
        {
            if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailView-568h" bundle:nil];
                
            }
            else
            {
                detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
            }
        }

        // ...
        
        NSDictionary *dataOfAvatar = [NSDictionary dictionaryWithObjectsAndKeys:arrayDescForAvatar,@"Desc", arrayIfYouForAvatar, @"IfYou", nameOfAvatarTextField.text, @"Name",nil];
        NSLog(@"dataOfAvatar = %@", dataOfAvatar);
        detailViewController.currentAvatarDictionary = dataOfAvatar;
        detailViewController.indexInListOfSavedFaces = indexInListOfSavedFaces;
        detailViewController.didPassedFromFaceReading = YES;
        // Pass the selected object to the new view controller.
        [self.navigationController pushViewController:detailViewController animated:YES];
        [detailViewController release];

    }
    
    return;

}



- (IBAction)menuButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
    
}

#pragma mark  conform UITextFieldDelegate 

-   (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    [self saveButtonDidTap:nil];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{   
   // self.scrollView.contentOffset = CGPointMake(0, nameOfAvatarTextField.frame.origin.y - 10);
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
        return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

#pragma mark  conform  UITableViewDataSource, UITableViewDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{   // Return the number of sections.
    //  In Our case "Description" & "If You ?"
    if (numberItemsInArrayIfYouForAvatar > 0)
    {
        return 2;
    }
    return 1;
}

- (NSInteger)       tableView:(UITableView *)tableView 
        numberOfRowsInSection:(NSInteger)section
{   // Return the number of rows in the section.
    
    if (section == 0)
    {
        // number items in all desc arrays
        return [arrayDescForAvatar count];
    }
    
    if (section == 1)
    { // number items in all IfYou dictionaries
              
        return [arrayIfYouForAvatar count]; 
    }
    
    return -1;
}

- (NSString *)      tableView:(UITableView *)tableViewVar
      titleForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return @"Description";
    }
    if (section == 1)
    {
        return @"Suggestions for them:";
    }
    return @"";
}

- (UITableViewCell *)tableView:(UITableView *)tableViewVar 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{  
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableViewVar dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle 
                                       reuseIdentifier:CellIdentifier]autorelease];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        cell.detailTextLabel.numberOfLines = 0;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    NSString *currentString = nil; 
    NSRange firstSubrange, secondSubrange;
    int length, location;
    
    if (indexPath.section == 0)
    {
        currentString = [arrayDescForAvatar objectAtIndex:indexPath.row];
       
    }
    if (indexPath.section == 1)
    {
        currentString = [arrayIfYouForAvatar objectAtIndex:indexPath.row];

    }
    
    firstSubrange = [currentString rangeOfString:@"<b>"];
    secondSubrange = [currentString rangeOfString:@"</b>"];
    location = firstSubrange.location + firstSubrange.length;
    length = secondSubrange.location - location;
    
    //  begin new block of code
    NSString *firstKey = [currentString substringWithRange:NSMakeRange(location, length)];
    NSString *secondKey = [firstKey stringByAppendingString:@"_DESC"];
    NSString *englishDescription = [currentString substringFromIndex:(secondSubrange.location + secondSubrange.length + 1)];
    DLog(@" firstKey: %@ secondKey: %@  englishDescription: %@", firstKey, secondKey, englishDescription);
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            cell.textLabel.text =  NSLocalizedString(firstKey, nil);
            cell.detailTextLabel.text = NSLocalizedString(secondKey, nil);
        }
        else
        {
            cell.textLabel.text =  firstKey;
            cell.detailTextLabel.text = englishDescription;
        }
    }
    else
    {
        cell.textLabel.text =  firstKey;
        cell.detailTextLabel.text = englishDescription;
    }
    
    DLog(@" %@",cell.textLabel.text );
    DLog(@" %@", cell.detailTextLabel.text);
    
    //  end new block of code
    
    if (indexPath.section == 0)
    {
         [namedFeatureDesc  insertObject:cell.textLabel.text atIndex:indexPath.row];
         [featureDesc  insertObject:cell.detailTextLabel.text atIndex:indexPath.row]; 
    }
   
    if (indexPath.section == 1)
    {
        [namedFeatureIfYou  insertObject:cell.textLabel.text atIndex:indexPath.row];
        [featureIfYou  insertObject:cell.detailTextLabel.text atIndex:indexPath.row];        
    }
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}

#pragma mark tableview delegate 

- (void)        tableView:(UITableView *)tableView 
  didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return;
    }

    [Sound soundEffect:1];
    RowTextViewController *rvc = nil;
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        rvc = [[RowTextViewController alloc] initWithNibName:@"RowTextView-568h" bundle:nil];
    }
    else
    {
        rvc = [[RowTextViewController alloc] initWithNibName:@"RowTextViewController" bundle:nil];
    }

    if (indexPath.section == 0)
    {
        rvc.featureString = [featureDesc objectAtIndex:indexPath.row] ;
        rvc.namedFeatureString = [namedFeatureDesc objectAtIndex:indexPath.row];
    }
    if (indexPath.section == 1)
    {
        rvc.featureString = [featureIfYou objectAtIndex:indexPath.row] ;
        rvc.namedFeatureString = [namedFeatureIfYou objectAtIndex:indexPath.row];
    }
    isCustomItemDidSelected = YES;
    //selectedIndexPath = indexPath;
    //[selectedIndexPath retain];
    savedOffset = [self.scrollView contentOffset];
    [self.navigationController pushViewController:rvc animated:YES];
    [rvc release];
}


#pragma mark hearder view in tableview delegate !!!


- (CGFloat)        tableView:(UITableView *)tableView_
    heightForHeaderInSection:(NSInteger)section

{
    CGFloat height = 10.0;
    if (tableView_ == self.tableView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            height = 61;
            //cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
        }
        else
        {
            height = 30.0;
        }
        
    }
    return height;
}



- (UIView *)    tableView:(UITableView *)tableView_
   viewForHeaderInSection:(NSInteger)section
{
    UIView *view = nil;
    
    if (tableView_ == self.tableView)
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            // iPad
            if (section == 0)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Description.png"]];
                view = descriptionView;
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  Description
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"Description", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: 47.0];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }

            }
            if (section == 1)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IfYouTry.png"]];
                view = descriptionView;
                // localizing ...
                
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  If You? Try…
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"If You? Try…", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: 47.0];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }

            }
            
        }
        else
        {
            // iPhone 
            if (section == 0)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Description-iPhone.png"]];
                view = descriptionView;
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  Description
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"Description", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: view.frame.size.height];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }
            }
            
            if (section == 1)
            {
                UIImageView * descriptionView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IfYouTry-iPhone.png"]];
                view = descriptionView;
                // localizing ...
                
                NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
                if (![currentLanguage isEqualToString:@"en"])
                {
                    if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
                    {
                        //  If You? Try…
                        
                        UILabel *labelIfYou = nil;
                        [(UIImageView *)view  setImage:nil];
                        labelIfYou = [[UILabel alloc] initWithFrame:view.frame];
                        labelIfYou.backgroundColor = [UIColor clearColor];
                        labelIfYou.text = NSLocalizedString(@"If You? Try…", nil);
                        labelIfYou.font = [UIFont boldSystemFontOfSize: view.frame.size.height];
                        labelIfYou.textColor = [UIColor redColor];
                        labelIfYou.shadowColor = [UIColor lightGrayColor];
                        labelIfYou.shadowOffset = CGSizeMake(2.0f, 2.0f);
                        [labelIfYou sizeToFit];
                        labelIfYou.textAlignment = NSTextAlignmentCenter;
                        [view addSubview:labelIfYou];
                        
                        [labelIfYou release];
                        labelIfYou = nil;
                    }
                }

            }
        }
        
    }
    
    return [view autorelease];
    
}



/*
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
   }

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView 
            editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
   }

- (void)        tableView:(UITableView *)tableViewVar 
       commitEditingStyle:(UITableViewCellEditingStyle)editingStyle 
        forRowAtIndexPath:(NSIndexPath *)indexPath
{
  
 }
*/ 


#pragma mark Keyboard notification using

- (void) handleKeyboardWillShow:(NSNotification *)paramNotification
{
    
    NSDictionary *userInfo = [paramNotification userInfo];
    
    NSValue *animationCurveObject =    [userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    NSValue *animationDurationObject =     [userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    NSValue *keyboardEndRectObject =     [userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    
    NSUInteger animationCurve = 0;
    double animationDuration = 0.0f;
    CGRect keyboardEndRect = CGRectMake(0, 0, 0, 0);
    
    [animationCurveObject getValue:&animationCurve];
    [animationDurationObject getValue:&animationDuration];
    [keyboardEndRectObject getValue:&keyboardEndRect];
    
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    
    /* Convert the frame from window's coordinate system to
     our view's coordinate system */
    keyboardEndRect = [self.view convertRect:keyboardEndRect
                                    fromView:window];
    
    [UIView beginAnimations:@"changeScrollViewContentInset"
                    context:NULL];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:(UIViewAnimationCurve)animationCurve];
    
    CGRect intersectionOfKeyboardRectAndWindowRect = CGRectIntersection(window.frame, keyboardEndRect);
    
    CGFloat bottomInset = intersectionOfKeyboardRectAndWindowRect.size.height;
    
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        self.scrollView.contentOffset = CGPointMake(0, 58.0);

    }
    else
    {
        self.scrollView.contentOffset = CGPointMake(0, bottomInset);
        //self.scrollView.contentOffset = CGPointMake(0, 58.0);
    }
    
    if (isCustomItemDidSelected)
    {
        //[self.tableView scrollToRowAtIndexPath:selectedIndexPath
        //                      atScrollPosition:UITableViewScrollPositionTop
        //                              animated:YES];
        isCustomItemDidSelected = NO;
        // restore:
        [self.scrollView setContentOffset:savedOffset];
    }
    
}

- (void) handleKeyboardWillHide:(NSNotification *)paramNotification
{
    
    NSDictionary *userInfo = [paramNotification userInfo];
    
    NSValue *animationCurveObject =  [userInfo valueForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    NSValue *animationDurationObject = [userInfo valueForKey:UIKeyboardAnimationDurationUserInfoKey];
    
    NSValue *keyboardEndRectObject = [userInfo valueForKey:UIKeyboardFrameEndUserInfoKey];
    
    NSUInteger animationCurve = 0;
    double animationDuration = 0.0f;
    CGRect keyboardEndRect = CGRectMake(0, 0, 0, 0);
    
    [animationCurveObject getValue:&animationCurve];
    [animationDurationObject getValue:&animationDuration];
    [keyboardEndRectObject getValue:&keyboardEndRect];
    
    [UIView beginAnimations:@"changeScrollViewContentInset"
                    context:NULL];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:(UIViewAnimationCurve)animationCurve];
    
    //self.scrollView.contentInset = UIEdgeInsetsZero;
    
    self.scrollView.contentOffset = CGPointMake(0, 0);
    
    [UIView commitAnimations];
    
    
}


#pragma mark IB action methods


- (IBAction)howToReadFacesButtonDidTap:(id)sender
{
    DLog(@" ");
   
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if ([currentLanguage isEqualToString:@"ja"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.keenonyou.com/How-To-Read-Faces.php?lan=jp"]];
    }
    else if ([currentLanguage isEqualToString:@"zh-Hans"])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.keenonyou.com/How-To-Read-Faces.php?lan=cn"]];
    }
    else
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.keenonyou.com/How-To-Read-Faces.php?lan=en"]];
    }
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Face Reading
    
    //  make image of button  nil (invisible)
    [self.faceReadingImageButton setImage:nil  forState:UIControlStateNormal];
    [self.faceReadingImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    CGRect faceReadingFrame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        faceReadingFrame = CGRectMake(155, 20, 459, 56);
    }
    else
    {
        faceReadingFrame = CGRectMake(20, 20, 280, 35);
    }
    label = [[UILabel alloc] initWithFrame:faceReadingFrame];
    label.backgroundColor = [UIColor clearColor];
    //DLog(@"Face Reading localized value: %@", NSLocalizedString(@"Face Reading", nil));
    label.text = NSLocalizedString(@"Face Reading", nil);
    
    label.font = [UIFont boldSystemFontOfSize: self.faceReadingImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.textAlignment = NSTextAlignmentCenter;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label.center = CGPointMake(384, 58);
    }
    else
    {
        label.center = CGPointMake(160, 35);
    }
    
    [self.scrollView addSubview:label];
    [label release];
    label = nil;
    
    // 2. Save
    
    [self.saveImageButton  setImage:nil forState:UIControlStateNormal];
    [self.saveImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    
    
    CGRect saveFrame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        saveFrame = CGRectMake(566, 852, 124, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            saveFrame = CGRectMake(226, 333, 82, 31);
        }
        else
        {
            saveFrame = CGRectMake(226, 333, 82, 31);
        }
        
    }
    
    label2 = [[UILabel alloc] initWithFrame:saveFrame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Save", nil);
    label2.font = [UIFont boldSystemFontOfSize: saveFrame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];

    

    [self.scrollView addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3. Menu
    
    [self.menuImageButton setImage:nil forState:UIControlStateNormal];
    [self.menuImageButton setTitle:@" " forState:UIControlStateNormal];
    CGRect frame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        frame = CGRectMake(571, 946, 123, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(255, 507, 82, 31);
        }
        else
        {
            frame = CGRectMake(255, 417, 82, 31);
        }
    }
    
    
    UILabel *labelMenu = [[UILabel alloc] initWithFrame:frame];
    labelMenu.backgroundColor = [UIColor clearColor];
    labelMenu.text = NSLocalizedString(@"Menu", nil);;
    labelMenu.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    labelMenu.textColor = [UIColor redColor];
    labelMenu.shadowColor = [UIColor lightGrayColor];
    labelMenu.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [labelMenu sizeToFit];
    //label4.center = frame.center;
    [self.view addSubview:labelMenu];
    [labelMenu release];
    labelMenu = nil;
    
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        frame = CGRectMake(91, 946, 123, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(60, 507, 82, 31);
        }
        else
        {
            frame = CGRectMake(60, 417, 82, 31);
        }
    }

    
    
    UILabel *label4 = [[UILabel alloc] initWithFrame:frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    //
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}


@end
