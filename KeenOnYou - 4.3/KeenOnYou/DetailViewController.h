//
//  DetailViewController.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController <UIAlertViewDelegate, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIButton *deleteFaceButton;

@property (assign, nonatomic) NSUInteger indexInListOfSavedFaces;
@property (retain, nonatomic) NSDictionary *currentAvatarDictionary;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;
@property (assign, nonatomic) BOOL didPassedFromFaceReading;
@property (retain, nonatomic) IBOutlet UIView *viewWithDisclaimerText;

//  Localizng ...
@property (retain, nonatomic) IBOutlet UIButton *faceReadingImageButton;
@property (retain, nonatomic) IBOutlet UIButton *deleteImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;
@property (retain, nonatomic) IBOutlet UIButton *menuImageButton;

- (IBAction)backButtobDidTap:(id)sender;

- (IBAction)menuButtonDidTap:(id)sender;

- (IBAction)deleteFaceDidTap:(id)sender;

- (IBAction)howToReadFacesDidTap:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *howToReadFacesLabel;

@end
