//
//  ListSavedFacesTableViewController.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListSavedFacesTableViewController.h"
#import "FileHelpers.h"
#import "ListSavedFaceCell.h"
#import "DetailViewController.h"

@implementation ListSavedFacesTableViewController

@synthesize tableView;
@synthesize listOfSavedFaces;
@synthesize didPassedFromFaceReading;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //  Localization of view
    
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }
}


- (void)viewDidUnload
{
    [self setTableView:nil];
    [self setFaceHistoryImageButton:nil];
    [self setMenuImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSString *fileNameListOfSavedFaces = pathInDocumentDirectory(@"listsavedfaces");
    self.listOfSavedFaces = [NSMutableArray arrayWithContentsOfFile:fileNameListOfSavedFaces];
    
    if (self.listOfSavedFaces == nil)
    {
        DLog(@"WE HAVE NOT SAVED FACES");
        return;
    }
    
    DLog(@" до сортировки  %@", self.listOfSavedFaces);
    
    // sorting 
    [self.listOfSavedFaces sortUsingComparator: ^(id obj1, id obj2)
    {
        return [[obj1 objectForKey:@"Name"] compare: [obj2 objectForKey:@"Name"]];
     }];
    
    DLog(@"после сортировки ...  %@", self.listOfSavedFaces);
    
    [tableView reloadData];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView 
 numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.listOfSavedFaces count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView 
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UITableViewCell* cell = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        NSArray* objs = [[NSBundle mainBundle] loadNibNamed:@"ListSavedFaceCell-iPad"
                                                      owner:self
                                                    options:nil];
        for (id curObj in objs)
        {
            if ([curObj isKindOfClass:[ListSavedFaceCell class]])
            {
                cell = (ListSavedFaceCell*)curObj;
            }
        }

        NSMutableString *nameFilePNG = [NSMutableString stringWithCapacity:0];
        NSString *name = [[listOfSavedFaces objectAtIndex:indexPath.row] objectForKey:@"Name"];
        //cell.textLabel.text = name;
        [nameFilePNG setString:name];
        [nameFilePNG insertString:@"small" atIndex:0];
        [nameFilePNG appendString:@".png"];
        //
        // NSLog(@"nameFilePNG = %@", nameFilePNG);
        
        NSString *path = pathInDocumentDirectory([nameFilePNG substringFromIndex:0]);
        
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        
        ((ListSavedFaceCell*)cell).nameAvatarLabel.text = name;
        ((ListSavedFaceCell*)cell).avatarView.image = image;
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        NSArray* objs = [[NSBundle mainBundle] loadNibNamed:@"ListSavedFaceCell"
                                                      owner:self
                                                    options:nil];
        for (id curObj in objs)
        {
            if ([curObj isKindOfClass:[ListSavedFaceCell class]])
            {
                cell = (ListSavedFaceCell*)curObj;
            }
        }
        
        NSMutableString *nameFilePNG = [NSMutableString stringWithCapacity:0];
        NSString *name = [[listOfSavedFaces objectAtIndex:indexPath.row] objectForKey:@"Name"];
        //cell.textLabel.text = name;
        [nameFilePNG setString:name];
        [nameFilePNG insertString:@"small" atIndex:0];
        [nameFilePNG appendString:@".png"];
        //
        // NSLog(@"nameFilePNG = %@", nameFilePNG);
        
        NSString *path = pathInDocumentDirectory([nameFilePNG substringFromIndex:0]);
        
        UIImage *image = [UIImage imageWithContentsOfFile:path];
        
        ((ListSavedFaceCell*)cell).nameAvatarLabel.text = name;
        ((ListSavedFaceCell*)cell).avatarView.image = image;
    }
    else
    {
        DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
    }
        
    cell.backgroundColor = [UIColor clearColor];
    cell.accessoryType  = UITableViewCellAccessoryNone;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView 
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    [Sound soundEffect:1];
    DetailViewController *detailViewController = nil;
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailView-iPad" bundle:nil];
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0)
        {
            if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                // iPhone 5 with height 568
                detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailView-568h" bundle:nil];
            }
            else
            {
                detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
            }
        }
        else
        {
            //iphone screen
            detailViewController = [[DetailViewController alloc] initWithNibName:@"DetailViewController" bundle:nil];
        }
    }
    else
    {
        DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
    }
    
    // ...
    detailViewController.currentAvatarDictionary = [self.listOfSavedFaces objectAtIndex:indexPath.row];
    detailViewController.indexInListOfSavedFaces = indexPath.row;
    detailViewController.didPassedFromFaceReading = NO;
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];
     [detailViewController release];
}

#pragma mark work with action






- (IBAction)backButtonDidTap:(id)sender 
{
    [Sound soundEffect:1];
    [self.navigationController popToRootViewControllerAnimated: YES];
}

- (IBAction)menuButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popToRootViewControllerAnimated: YES];
}

#pragma mark memory management

     - (void)dealloc
{
    [tableView release];
    [_faceHistoryImageButton release];
    [_menuImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Face History
    
    //  make image of button  nil (invisible)
    [self.faceHistoryImageButton setImage:nil  forState:UIControlStateNormal];
    [self.faceHistoryImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.faceHistoryImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Face History", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.faceHistoryImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.faceHistoryImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Menu
    
    [self.menuImageButton setImage:nil forState:UIControlStateNormal];
    [self.menuImageButton setTitle:@" " forState:UIControlStateNormal];
    CGRect frame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
        if (![currentLanguage isEqualToString:@"en"])
        {
            if ([currentLanguage isEqualToString:@"ja"])
            {
                frame = CGRectMake(560.0, 946.0, 123.0, 47.0);
                CGRect menuButtonFrame = self.menuImageButton.frame;
                menuButtonFrame = CGRectMake(menuButtonFrame.origin.x - 80.0 , menuButtonFrame.origin.y, menuButtonFrame.size.width + 80.0, menuButtonFrame.size.height);
                self.menuImageButton.frame = menuButtonFrame;
            }
            if ([currentLanguage isEqualToString:@"zh-Hans"])
            {
                frame = CGRectMake(640.0, 946.0, 123.0, 47.0);
            }
        }
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(255, 495, 82, 31);
        }
        else
        {
            frame = CGRectMake(255, 418, 82, 31);
        }
    }
    
    
    UILabel *labelMenu = [[UILabel alloc] initWithFrame:frame];
    labelMenu.backgroundColor = [UIColor clearColor];
    labelMenu.text = NSLocalizedString(@"Menu", nil);;
    labelMenu.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    labelMenu.textColor = [UIColor redColor];
    labelMenu.shadowColor = [UIColor lightGrayColor];
    labelMenu.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [labelMenu sizeToFit];
    //label4.center = frame.center;
    [self.view addSubview:labelMenu];
    [labelMenu release];
    labelMenu = nil;
    
    
           
    // 3. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    //CGRect frame;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        frame = CGRectMake(91, 946, 123, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(60, 495, 82, 31);
        }
        else
        {
            frame = CGRectMake(60, 418, 82, 31);
        }
    }
    
    
    UILabel *labelBack = [[UILabel alloc] initWithFrame:frame];
    labelBack.backgroundColor = [UIColor clearColor];
    labelBack.text = NSLocalizedString(@"Back", nil);;
    labelBack.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    labelBack.textColor = [UIColor redColor];
    labelBack.shadowColor = [UIColor lightGrayColor];
    labelBack.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [labelBack sizeToFit];
    //label4.center = frame.center;
    [self.view addSubview:labelBack];
    [labelBack release];
    labelBack = nil;
    
}


@end
