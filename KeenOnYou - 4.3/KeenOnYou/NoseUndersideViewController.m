//
//  NoseUndersideViewController.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NoseUndersideViewController.h"
#import "ReadFace.h"
#import "ChinViewController.h"

@implementation NoseUndersideViewController

@synthesize currentReadFace;

@synthesize angledUpButton;
@synthesize straightButton;
@synthesize angledDownButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        [self.angledUpButton setImage:[UIImage imageNamed:@"NoseUnderside_Male_AngledUp.png"] forState:UIControlStateNormal];
        [self.straightButton setImage:[UIImage imageNamed:@"NoseUnderside_Male_Straight.png"] forState:UIControlStateNormal]; 
        [self.angledDownButton setImage:[UIImage imageNamed:@"NoseUnderside_Male_AngledDown.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"])
    {
        [self.angledUpButton setImage:[UIImage imageNamed:@"NoseUnderside_Female_AngledUp.png"] forState:UIControlStateNormal];
        [self.straightButton setImage:[UIImage imageNamed:@"NoseUnderside_Female_Straight.png"] forState:UIControlStateNormal]; 
        [self.angledDownButton setImage:[UIImage imageNamed:@"NoseUnderside_Female_AngledDown.png"] forState:UIControlStateNormal];        
    }
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }

}

- (void)viewDidUnload
{
    [self setAngledUpButton:nil];
    [self setStraightButton:nil];
    [self setAngledDownButton:nil];
    
    [self setNoseUndersideImageButton:nil];
    [self setAngledUpImageButton:nil];
    [self setStraightImageButton:nil];
    [self setAngledDownImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc
{
    [angledUpButton release];
    [straightButton release];
    [angledDownButton release];
    
    [_noseUndersideImageButton release];
    [_angledUpImageButton release];
    [_straightImageButton release];
    [_angledDownImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark IB Action in use !!!

-(ChinViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    ChinViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[ChinViewController alloc] initWithNibName:@"ChinView-iPad"
                                                      bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[ChinViewController alloc] initWithNibName: @"ChinView-568h"
                                                                  bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[ChinViewController alloc] initWithNibName:@"ChinViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[ChinViewController alloc] initWithNibName:@"ChinViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
    
}

- (IBAction)angledUpButtonDidTap:(id)sender
{
    self.currentReadFace.noseUnderside = @"Up";
    [Sound soundEffect:1];
    
    ChinViewController *cvc= [self newTrueView];
        
    cvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];
}

- (IBAction)straightButtonDidTap:(id)sender { 
    [Sound soundEffect:1];
    self.currentReadFace.noseUnderside = @"Straight";
    
    ChinViewController *cvc= [self newTrueView];
    
    cvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];
}

- (IBAction)angledDownButtonDidTap:(id)sender {
    [Sound soundEffect:1];
    self.currentReadFace.noseUnderside = @"Down";
    
    ChinViewController *cvc= [self newTrueView];
    
    cvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:cvc animated:YES];
    [cvc release];  
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];    
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Nose Underside
    
    //  make image of button  nil (invisible)
    [self.noseUndersideImageButton setImage:nil  forState:UIControlStateNormal];
    [self.noseUndersideImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.noseUndersideImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Nose Underside", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.noseUndersideImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.noseUndersideImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Angled Up Nose Underside
    
    [self.angledUpImageButton  setImage:nil forState:UIControlStateNormal];
    [self.angledUpImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.angledUpImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Angled Up Nose Underside", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.angledUpImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.angledUpImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.angledUpImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.angledUpImageButton.center.x, self.angledUpImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Straight Nose Underside
    
    [self.straightImageButton setImage:nil    forState:UIControlStateNormal];
    [self.straightImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.straightImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Straight Nose Underside", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.straightImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.straightImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.straightImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.straightImageButton.center.x, self.straightImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.  Angled Down Nose Underside
    
    [self.angledDownImageButton setImage:nil    forState:UIControlStateNormal];
    [self.angledDownImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.angledDownImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Angled Down Nose Underside", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.angledDownImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.angledDownImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.angledDownImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.angledDownImageButton.center.x, self.angledDownImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}



@end
