//
//  EyebrowHeightViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EyebrowHeightViewController.h"
#import "ReadFace.h"
#import "EyeSpacingViewController.h"

@implementation EyebrowHeightViewController

@synthesize currentReadFace;

@synthesize highButton;
@synthesize averageButton;
@synthesize lowButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        [self.highButton setImage:[UIImage imageNamed:@"EyebrowHeight_Male_High.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"EyebrowHeight_Male_Average.png"] forState:UIControlStateNormal]; 
        [self.lowButton setImage:[UIImage imageNamed:@"EyebrowHeight_Male_Low.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"]) {
        [self.highButton setImage:[UIImage imageNamed:@"EyebrowHeight_Female_High.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"EyebrowHeight_Female_Average.png"] forState:UIControlStateNormal]; 
        [self.lowButton setImage:[UIImage imageNamed:@"EyebrowHeight_Female_Low.png"] forState:UIControlStateNormal];         
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
        
    }

    
}

- (void)viewDidUnload
{
    [self setHighButton:nil];
    [self setAverageButton:nil];
    [self setLowButton:nil];
    [self setEyebrowHeightImageButton:nil];
    [self setHighImageButton:nil];
    [self setAverageImageButton:nil];
    [self setLowImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark memory management

- (void)dealloc
{
    [highButton release];
    [averageButton release];
    [lowButton release];
    [_eyebrowHeightImageButton release];
    [_highImageButton release];
    [_averageImageButton release];
    [_lowImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions

-(EyeSpacingViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    EyeSpacingViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[EyeSpacingViewController alloc] initWithNibName:@"EyeSpacingView-iPad"
                                                             bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[EyeSpacingViewController alloc] initWithNibName: @"EyeSpacingView-568h"
                                                                         bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[EyeSpacingViewController alloc] initWithNibName:@"EyeSpacingViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[EyeSpacingViewController alloc] initWithNibName:@"EyeSpacingViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
    
}



- (IBAction)highButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowHeight = @"High";
    EyeSpacingViewController *esvc= [self newTrueView];
    
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release];   
}

- (IBAction)averageButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowHeight = @"Average";
    EyeSpacingViewController *esvc= [self newTrueView];
    
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release];     
}

- (IBAction)lowButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowHeight = @"Low";
    EyeSpacingViewController *esvc = [self newTrueView];
    
    esvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:esvc animated:YES];
    [esvc release]; 
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Eyebrow Height
    
    //  make image of button  nil (invisible)
    [self.eyebrowHeightImageButton setImage:nil  forState:UIControlStateNormal];
    [self.eyebrowHeightImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.eyebrowHeightImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Eyebrow Height", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.eyebrowHeightImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.eyebrowHeightImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. High Eyebrow Height
    
    [self.highImageButton  setImage:nil forState:UIControlStateNormal];
    [self.highImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.highImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"High Eyebrow Height", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.highImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.highImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.highImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.highImageButton.center.x, self.highImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Average Eyebrow Height
    
    [self.averageImageButton setImage:nil    forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Average Eyebrow Height", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.averageImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.averageImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.averageImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.   Low Eyebrow Height    
    [self.lowImageButton setImage:nil    forState:UIControlStateNormal];
    [self.lowImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.lowImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Low Eyebrow Height", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.lowImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.lowImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.lowImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.lowImageButton.center.x, self.lowImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}



@end
