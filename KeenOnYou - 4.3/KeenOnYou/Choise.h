//
//  Choise.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Choise : NSObject

@property (nonatomic, retain) NSString *art;
@property (nonatomic, retain) NSMutableArray *desc;
@property (nonatomic, retain) NSString *ifYou;
           

- (id) initWithArt: (NSString *) art
              desc: (NSMutableArray *) desc
             ifYou: (NSString *) ifYou;

@end
