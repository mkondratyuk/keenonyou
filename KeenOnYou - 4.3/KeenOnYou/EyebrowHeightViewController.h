//
//  EyebrowHeightViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface EyebrowHeightViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *highButton;
@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *lowButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *eyebrowHeightImageButton;
@property (retain, nonatomic) IBOutlet UIButton *highImageButton;
@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *lowImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;



- (IBAction)highButtonDidTap:(id)sender;
- (IBAction)averageButtonDidTap:(id)sender;
- (IBAction)lowButtonDidTap:(id)sender;

- (IBAction)backButtonDidTap:(id)sender;


@end
