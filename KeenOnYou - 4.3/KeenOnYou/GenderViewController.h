//
//  GenderViewController.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface GenderViewController : UIViewController

@property (nonatomic, assign) BOOL loadFromMenuViewController;

@property (retain, nonatomic) IBOutlet UIButton *mailTextButton;
@property (retain, nonatomic) IBOutlet UIButton *femaleTextButton;
@property (retain, nonatomic) IBOutlet UIButton *backButton;
@property (retain, nonatomic) IBOutlet UIButton *genderTextButton;

- (IBAction)mailButtonDidTap:(id)sender;
- (IBAction)femailButtonDidTap:(id)sender;
- (IBAction)backButtonDidTap:(id)sender;

@end
