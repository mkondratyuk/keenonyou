//
//  Eyebrow ShapesViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EyebrowShapeViewController.h"
#import "ReadFace.h"
#import "EyebrowHeightViewController.h"

@implementation EyebrowShapeViewController

@synthesize currentReadFace;

@synthesize averageButton;
@synthesize semiCircleButton;
@synthesize thickButton;
@synthesize sharpPointButton;
@synthesize straightButton;
@synthesize highArcButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"])
    {
        [self.averageButton setImage:[UIImage imageNamed:@"EyeBrows_Male_Average.png"] forState:UIControlStateNormal];
        [self.semiCircleButton setImage:[UIImage imageNamed:@"EyeBrows_Male_Semi-Circle.png"] forState:UIControlStateNormal];
        
        [self.thickButton setImage:[UIImage imageNamed:@"EyeBrows_Male_Think.png"] forState:UIControlStateNormal];
        [self.straightButton setImage:[UIImage imageNamed:@"EyeBrows_Male_Straight.png"] forState:UIControlStateNormal];
        [self.sharpPointButton setImage:[UIImage imageNamed:@"EyeBrows_Male_SharpPoint.png"] forState:UIControlStateNormal];
        [self.highArcButton setImage:[UIImage imageNamed:@"EyeBrows_Male_HighArc.png"] forState:UIControlStateNormal];      
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"])
    {
        [self.sharpPointButton setImage:[UIImage imageNamed:@"EyeBrows_Female_SharpPoint.png"] forState:UIControlStateNormal];
        [self.thickButton setImage:[UIImage imageNamed:@"EyeBrows_Female_Think.png"] forState:UIControlStateNormal];
        
        [self.averageButton setImage:[UIImage imageNamed:@"EyeBrows_Female_Average.png"] forState:UIControlStateNormal];
        [self.semiCircleButton setImage:[UIImage imageNamed:@"EyeBrows_Female_Semi-Circle.png"] forState:UIControlStateNormal];
        
        [self.highArcButton setImage:[UIImage imageNamed:@"EyeBrows_Female_HighArc.png"] forState:UIControlStateNormal];
        [self.straightButton setImage:[UIImage imageNamed:@"EyeBrows_Female_Straight.png"] forState:UIControlStateNormal];        
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            if ([currentLanguage isEqualToString:@"ja"] )
            {
                [self localizingThroughAddingLabels: @"ja"];
            }
            else if ([currentLanguage isEqualToString:@"zh-Hans"])
            {
                [self localizingThroughAddingLabels: @"zh-Hans"];
            }
            else
            {
                DLog(@"something wrong ...");
            }
                
            
        }
        
    }

}

- (void)viewDidUnload
{
    [self setAverageButton:nil];
    [self setSemiCircleButton:nil];
    [self setThickButton:nil];
    [self setSharpPointButton:nil];
    [self setStraightButton:nil];
    [self setHighArcButton:nil];

    [self setEyebrowShapeImageButton:nil];
    [self setAverageImageButton:nil];
    [self setSemiCircleImageButton:nil];
    [self setThickImageButton:nil];
    [self setSharpPointImageButton:nil];
    [self setStraightImageButton:nil];
    [self setHighArcImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark memory management

- (void)dealloc
{
    [averageButton release];
    [semiCircleButton release];
    [thickButton release];
    [sharpPointButton release];
    [straightButton release];
    [highArcButton release];
    
    [_eyebrowShapeImageButton release];
    [_averageImageButton release];
    [_semiCircleImageButton release];
    [_thickImageButton release];
    [_sharpPointImageButton release];
    [_straightImageButton release];
    [_highArcImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions


-(EyebrowHeightViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    EyebrowHeightViewController *ehvc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        ehvc = [[EyebrowHeightViewController alloc] initWithNibName:@"EyebrowHeightView-iPad"
                                                            bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    ehvc = [[EyebrowHeightViewController alloc] initWithNibName: @"EyebrowHeightView-568h" 
                                                                        bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    ehvc = [[EyebrowHeightViewController alloc] initWithNibName:@"EyebrowHeightViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                ehvc = [[EyebrowHeightViewController alloc] initWithNibName:@"EyebrowHeightViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return ehvc;
    
}


- (IBAction)averageButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowShape = @"Average";
    EyebrowHeightViewController *ehvc= [self newTrueView ];
    ehvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:ehvc animated:YES];
    [ehvc release];     
}

- (IBAction)semiCircleButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowShape = @"SemiCircle";
    EyebrowHeightViewController *ehvc= [self newTrueView];
    ehvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:ehvc animated:YES];
    [ehvc release];  
}

- (IBAction)thickButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowShape = @"Thick";
    EyebrowHeightViewController *ehvc= [self newTrueView ];
    ehvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:ehvc animated:YES];
    [ehvc release];  
}

- (IBAction)sharpPointButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowShape = @"SharpPoint";
    EyebrowHeightViewController *ehvc= [self newTrueView ];
    ehvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:ehvc animated:YES];
    [ehvc release];  
}

- (IBAction)straitPointButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowShape = @"Straight";
    EyebrowHeightViewController *ehvc= [self newTrueView ];

    ehvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:ehvc animated:YES];
    [ehvc release];      
}

- (IBAction)highArcButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.eyebrowShape = @"HighArc";
    EyebrowHeightViewController *ehvc= [self newTrueView ];

    ehvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:ehvc animated:YES];
    [ehvc release];  
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) localizingThroughAddingLabels: (NSString *) flag
{
   
    // 1. Eyebrow Shapes
    
    //  make image of button  nil (invisible)
    [self.eyebrowShapeImageButton setImage:nil  forState:UIControlStateNormal];
    [self.eyebrowShapeImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.eyebrowShapeImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Eyebrow Shapes", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.eyebrowShapeImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.eyebrowShapeImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Average
    
    [self.averageImageButton  setImage:nil forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Average Eyebrow Shapes", nil);
    CGFloat height = self.averageImageButton.frame.size.height;
    label2.font = [UIFont boldSystemFontOfSize: height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.averageImageButton.center;
    }
    else
    {
        if ([flag isEqualToString:@"ja"])
        {
            label2.font = [UIFont boldSystemFontOfSize: 15];
        }
        else if ([flag isEqualToString:@"zh-Hans"])
        {
           // label2.font = [UIFont boldSystemFontOfSize: 25];
        }
        
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.averageImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
    }
    label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Thick
    
    [self.thickImageButton setImage:nil    forState:UIControlStateNormal];
    [self.thickImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.thickImageButton .frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Thick Eyebrow Shapes", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.thickImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.thickImageButton.center;
    }
    else
    {       
        if ([flag isEqualToString:@"ja"])
        {
            label3.font = [UIFont boldSystemFontOfSize: 15];
        }
        else if ([flag isEqualToString:@"zh-Hans"])
        {
            //label3.font = [UIFont boldSystemFontOfSize: 25];
        }

        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.thickImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.thickImageButton.center.x, self.thickImageButton.center.y);
            
        }
        
    }
    label3.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.   Straight
    
    [self.straightImageButton setImage:nil    forState:UIControlStateNormal];
    [self.straightImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.straightImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Straight Eyebrow Shapes", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.straightImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.straightImageButton.center;
    }
    else
    {
        if ([flag isEqualToString:@"ja"])
        {
            label5.font = [UIFont boldSystemFontOfSize: 15];
        }
        else if ([flag isEqualToString:@"zh-Hans"])
        {
           // label5.font = [UIFont boldSystemFontOfSize: 25];
        }

        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.straightImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.straightImageButton.center.x, self.straightImageButton.center.y);
        }
    }
    label5.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
    // 6.   Sharp Point
    
    [self.sharpPointImageButton setImage:nil    forState:UIControlStateNormal];
    [self.sharpPointImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label6 = nil;
    label6 = [[UILabel alloc] initWithFrame:self.sharpPointImageButton.frame];
    label6.backgroundColor = [UIColor clearColor];
    label6.text = NSLocalizedString(@"Sharp Point Eyebrow Shapes", nil);;
    label6.font = [UIFont boldSystemFontOfSize: self.sharpPointImageButton.frame.size.height];
    label6.textColor = [UIColor redColor];
    label6.shadowColor = [UIColor lightGrayColor];
    label6.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label6 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label6.center = self.sharpPointImageButton.center;
    }
    else
    {
        if ([flag isEqualToString:@"ja"])
        {
            label6.font = [UIFont boldSystemFontOfSize: 15];
        }
        else if ([flag isEqualToString:@"zh-Hans"])
        {
            //label6.font = [UIFont boldSystemFontOfSize: 25];
        }

        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label6.center = self.sharpPointImageButton.center;
        }
        else
        {
            label6.center = CGPointMake(self.sharpPointImageButton.center.x, self.sharpPointImageButton.center.y);
        }
    }
    
    label6.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label6];
    [label6 release];
    label6 = nil;
    
    // 7.  Semi-Circle
    
    [self.semiCircleImageButton setImage:nil    forState:UIControlStateNormal];
    [self.semiCircleImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label7 = nil;
    label7 = [[UILabel alloc] initWithFrame:self.semiCircleImageButton.frame];
    label7.backgroundColor = [UIColor clearColor];
    label7.text = NSLocalizedString(@"Semi-Circle Eyebrow Shapes", nil);;
    label7.font = [UIFont boldSystemFontOfSize: self.semiCircleImageButton.frame.size.height];
    label7.textColor = [UIColor redColor];
    label7.shadowColor = [UIColor lightGrayColor];
    label7.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label7 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label7.center = self.semiCircleImageButton.center;
    }
    else
    {
        if ([flag isEqualToString:@"ja"])
        {
            label7.font = [UIFont boldSystemFontOfSize: 15];
        }
        else if ([flag isEqualToString:@"zh-Hans"])
        {
           // label7.font = [UIFont boldSystemFontOfSize: 25];
        }
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label7.center = self.semiCircleImageButton.center;
        }
        else
        {
            label7.center = CGPointMake(self.semiCircleImageButton.center.x, self.semiCircleImageButton.center.y );
        }
    }
    
    label7.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label7];
    [label7 release];
    label7 = nil;
    
    // 8.   High Arc
    
    [self.highArcImageButton setImage:nil    forState:UIControlStateNormal];
    [self.highArcImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label8 = nil;
    label8 = [[UILabel alloc] initWithFrame:self.highArcImageButton.frame];
    label8.backgroundColor = [UIColor clearColor];
    label8.text = NSLocalizedString(@"High Arc Eyebrow Shapes", nil);;
    label8.font = [UIFont boldSystemFontOfSize: self.highArcImageButton.frame.size.height];
    label8.textColor = [UIColor redColor];
    label8.shadowColor = [UIColor lightGrayColor];
    label8.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label8 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label8.center = self.highArcImageButton.center;
    }
    else
    {
        if ([flag isEqualToString:@"ja"])
        {
            label8.font = [UIFont boldSystemFontOfSize: 15];
        }
        else if ([flag isEqualToString:@"zh-Hans"])
        {
           // label8.font = [UIFont boldSystemFontOfSize: 25];
        }

        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label8.center = self.highArcImageButton.center;
        }
        else
        {
            label8.center = CGPointMake(self.highArcImageButton.center.x, self.highArcImageButton.center.y);
        }
    }
    
    label8.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label8];
    [label8 release];
    label8 = nil;
}



@end

