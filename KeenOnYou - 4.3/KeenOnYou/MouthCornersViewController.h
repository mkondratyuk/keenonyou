//
//  MouthCornersViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface MouthCornersViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *turnsUpButton;
@property (retain, nonatomic) IBOutlet UIButton *straightButton;
@property (retain, nonatomic) IBOutlet UIButton *turnsDownButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *mouthCornersImageButton;
@property (retain, nonatomic) IBOutlet UIButton *turnUpImageButton;
@property (retain, nonatomic) IBOutlet UIButton *straightImageButton;
@property (retain, nonatomic) IBOutlet UIButton *turnsDownImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)turnsUpButtonDidTap:(id)sender;
- (IBAction)straightButtonDidTap:(id)sender;
- (IBAction)turnsDownButtonDidTap:(id)sender;

- (IBAction)backButtonDidTap:(id)sender;

@end
