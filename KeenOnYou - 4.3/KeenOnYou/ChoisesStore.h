//
//  ChoisesStore.h
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ChoisesStore : NSObject

@property (nonatomic, retain) NSArray *allChoices; 

+ (ChoisesStore *) defaultStore;


@end
