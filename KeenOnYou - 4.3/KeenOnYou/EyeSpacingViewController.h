//
//  EyeSpacingViewController.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface EyeSpacingViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *wideButton;
@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *closeButton;

//  for localizing image/text

@property (retain, nonatomic) IBOutlet UIButton *eyeSpacingImageButton;
@property (retain, nonatomic) IBOutlet UIButton *wideImageButton;

@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *closeImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)wideButtonDidTap:(id)sender;
- (IBAction)averageButtonDidTap:(id)sender;
- (IBAction)closeButtonDidTap:(id)sender;

- (IBAction)backButtonDidTap:(id)sender;


@end
