//
//  FaceShape.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface FaceShape : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *LongAndThinButton;
@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *wideButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *faceShapeImageButton;
@property (retain, nonatomic) IBOutlet UIButton *longAndThinImageButton;
@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *wideImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)longAndThinDidTap:(id)sender;
- (IBAction)averageDidTap:(id)sender;
- (IBAction)wideFaceDidTap:(id)sender;
- (IBAction)backButtonDidTap:(id)sender;

@end
