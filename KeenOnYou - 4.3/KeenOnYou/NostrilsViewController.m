//
//  NostrilsViewController.m
//  KeenOnYou
//
//  Created by Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "NostrilsViewController.h"
#import "ReadFace.h"
#import "NoseUndersideViewController.h"

@implementation NostrilsViewController

@synthesize currentReadFace;

@synthesize wideButton;
@synthesize averageButton;
@synthesize narrowButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"]) {
        [self.wideButton setImage:[UIImage imageNamed:@"Nostrils_Male_Wide.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"Nostrils_Male_Average.png"] forState:UIControlStateNormal]; 
        [self.narrowButton setImage:[UIImage imageNamed:@"Nostrils_Male_Narrow.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"]) {
        [self.wideButton setImage:[UIImage imageNamed:@"Nostrils_Female_Wide.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"Nostrils_Female_Average.png"] forState:UIControlStateNormal]; 
        [self.narrowButton setImage:[UIImage imageNamed:@"Nostrils_Female_Narrow.png"] forState:UIControlStateNormal];         
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }

}

- (void)viewDidUnload
{
    [self setWideButton:nil];
    [self setAverageButton:nil];
    [self setNarrowButton:nil];
    
    [self setNostrilsImageButton:nil];
    [self setWideImageButtob:nil];
    [self setAverageImageButton:nil];
    [self setNarrowImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark memory management

- (void)dealloc {
    [wideButton release];
    [averageButton release];
    [narrowButton release];
    
    [_nostrilsImageButton release];
    [_wideImageButtob release];
    [_averageImageButton release];
    [_narrowImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions


-(NoseUndersideViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    NoseUndersideViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[NoseUndersideViewController alloc] initWithNibName:@"NoseUndersideView-iPad"
                                                      bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[NoseUndersideViewController alloc] initWithNibName: @"NoseUndersideView-568h"
                                                                  bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[NoseUndersideViewController alloc] initWithNibName:@"NoseUndersideViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[NoseUndersideViewController alloc] initWithNibName:@"NoseUndersideViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
    
}


- (IBAction)wideButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.nostrils = @"Wide";

    NoseUndersideViewController *nuvc= [self newTrueView];

    nuvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:nuvc animated:YES];
    [nuvc release];
     
}

- (IBAction)averageButtonDidTap:(id)sender
{
    self.currentReadFace.nostrils = @"Average";
    [Sound soundEffect:1];
    
    NoseUndersideViewController *nuvc= [self newTrueView];
    
    nuvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:nuvc animated:YES];
    [nuvc release];
     
}

- (IBAction)narrowButtonDidTap:(id)sender {
    self.currentReadFace.nostrils = @"Narrow";
    [Sound soundEffect:1];
    
    NoseUndersideViewController *nuvc= [self newTrueView];
    
    nuvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:nuvc animated:YES];
    [nuvc release];
    
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Nostrils
    
    //  make image of button  nil (invisible)
    [self.nostrilsImageButton setImage:nil  forState:UIControlStateNormal];
    [self.nostrilsImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.nostrilsImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Nostrils", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.nostrilsImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.nostrilsImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. Wide Nostrils
    
    [self.wideImageButtob  setImage:nil forState:UIControlStateNormal];
    [self.wideImageButtob setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.wideImageButtob.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"Wide Nostrils", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.wideImageButtob.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.wideImageButtob.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.wideImageButtob.center;
        }
        else
        {
            label2.center = CGPointMake(self.wideImageButtob.center.x, self.wideImageButtob.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Average Nostrils
    [self.averageImageButton setImage:nil    forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Average Nostrils", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.averageImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.averageImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.averageImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5. Narrow Nostrils
    
    [self.narrowImageButton setImage:nil    forState:UIControlStateNormal];
    [self.narrowImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.narrowImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Narrow Nostrils", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.narrowImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.narrowImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.narrowImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.narrowImageButton.center.x, self.narrowImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}


@end
