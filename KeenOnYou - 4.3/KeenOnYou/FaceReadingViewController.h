//
//  FaceReadingViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@class ReadFace;

@interface FaceReadingViewController : UIViewController <UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UITextField *nameOfAvatarTextField;

@property (retain, nonatomic) IBOutlet UIImageView *backgraundImageViewForAvatar;

@property (retain, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, retain) ReadFace *currentReadFace;


@property (nonatomic, retain)  NSMutableArray *arrayDescForAvatar;
@property (nonatomic, assign)  NSUInteger numberItemsInArrayDescForAvatar;
@property (nonatomic, retain)  NSMutableArray *arrayIfYouForAvatar;
@property (nonatomic, assign) NSUInteger numberItemsInArrayIfYouForAvatar;

@property (retain, nonatomic) IBOutlet UIView *viewWithDisclaimmerText;

//  for localization ...

@property (retain, nonatomic) IBOutlet UIButton *faceReadingImageButton;
@property (retain, nonatomic) IBOutlet UIButton *saveImageButton;
@property (retain, nonatomic) IBOutlet UIButton *menuImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;
//  ???
@property (retain, nonatomic) IBOutlet UILabel *disclaimerLabel;
@property (retain, nonatomic) IBOutlet UILabel *firstLineLabel;
@property (retain, nonatomic) IBOutlet UILabel *secondLineLabel;
@property (retain, nonatomic) IBOutlet UILabel *thirdLineLabel;
@property (retain, nonatomic) IBOutlet UILabel *fourthLineLabel;

//  action 

- (IBAction)saveButtonDidTap:(id)sender;
//- (void) noButtonDidTap;
- (IBAction)menuButtonDidTap:(id)sender;
- (IBAction)backButtonDidTap:(id)sender;

@property (retain, nonatomic) IBOutlet UILabel *howToReadFacesLabel;
- (IBAction)howToReadFacesButtonDidTap:(id)sender;

@end
