//
//  NostrilsViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface NostrilsViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *wideButton;
@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *narrowButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *nostrilsImageButton;
@property (retain, nonatomic) IBOutlet UIButton *wideImageButtob;
@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *narrowImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)wideButtonDidTap:(id)sender;
- (IBAction)averageButtonDidTap:(id)sender;
- (IBAction)narrowButtonDidTap:(id)sender;


- (IBAction)backButtonDidTap:(id)sender;

@end
