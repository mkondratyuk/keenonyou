//
//  CheckbonesViewController.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CheckbonesViewController.h"
#import "ReadFace.h"
#import "NostrilsViewController.h"

@implementation CheckbonesViewController

@synthesize currentReadFace;

@synthesize highOrDefinedButton;
@synthesize averageButton;
@synthesize softButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.currentReadFace.gender isEqualToString:@"Mail"]) {
        [self.highOrDefinedButton setImage:[UIImage imageNamed:@"CheekBones_Male_High-Defined.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"CheekBones_Male_Average.png"] forState:UIControlStateNormal]; 
        [self.softButton setImage:[UIImage imageNamed:@"CheekBones_Male_Soft.png"] forState:UIControlStateNormal];        
    }
    if ([self.currentReadFace.gender isEqualToString:@"Femail"]) {
        [self.highOrDefinedButton setImage:[UIImage imageNamed:@"CheekBones_Female_High-Defined.png"] forState:UIControlStateNormal];
        [self.averageButton setImage:[UIImage imageNamed:@"CheekBones_Female_Average.png"] forState:UIControlStateNormal]; 
        [self.softButton setImage:[UIImage imageNamed:@"CheekBones_Female_Soft.png"] forState:UIControlStateNormal];        
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }
}

- (void)viewDidUnload
{
    [self setHighOrDefinedButton:nil];
    [self setAverageButton:nil];
    [self setSoftButton:nil];
    
    [self setCheekbonesImageButton:nil];
    [self setHighOrDefinedImageButton:nil];
    [self setAverageImageButton:nil];
    [self setSoftImageButton:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark memory management

- (void)dealloc {
    [highOrDefinedButton release];
    [averageButton release];
    [softButton release];
    
    [_cheekbonesImageButton release];
    [_highOrDefinedImageButton release];
    [_averageImageButton release];
    [_softImageButton release];
    [_backImageButton release];
    [super dealloc];
}

#pragma mark work with actions

-(NostrilsViewController *) newTrueView
{
    //  block for load variouse views (iPad & iPhone 5)
    NostrilsViewController *vc = nil;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        vc = [[NostrilsViewController alloc] initWithNibName:@"NostrilsView-iPad"
                                                        bundle:nil];
    }
    else
    {
        if ([UIScreen instancesRespondToSelector:@selector(scale)])
        {
            CGFloat scale = [[UIScreen mainScreen] scale];
            if (scale > 1.0)
            {
                if([[UIScreen mainScreen] bounds].size.height == 568)
                {
                    //iphone 5
                    vc = [[NostrilsViewController alloc] initWithNibName: @"NostrilsView-568h"
                                                                    bundle: nil];
                }
                else
                {
                    //iphone retina screen
                    vc = [[NostrilsViewController alloc] initWithNibName:@"NostrilsViewController" bundle:nil];
                }
            }
            else
            {
                //iphone screen
                vc = [[NostrilsViewController alloc] initWithNibName:@"NostrilsViewController" bundle:nil];
            }
        }
        else
        {
            DLog(@" ");
        }
    }
    //  end block
    return vc;
    
}



- (IBAction)highOrDefineButtoDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.cheeckbones = @"HighDef";

    NostrilsViewController *nvc= [self newTrueView];

    nvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:nvc animated:YES];
    [nvc release];
}
- (IBAction)averageButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.cheeckbones = @"Average";
    
    NostrilsViewController *nvc= [self newTrueView];
    
    nvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:nvc animated:YES];
    [nvc release];
}

- (IBAction)softButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    self.currentReadFace.cheeckbones = @"Soft";
    
     NostrilsViewController *nvc= [self newTrueView];
    
    nvc.currentReadFace = self.currentReadFace;
    [self.navigationController pushViewController:nvc animated:YES];
    [nvc release];
}

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    // 1. Cheekbones
    
    //  make image of button  nil (invisible)
    [self.cheekbonesImageButton setImage:nil  forState:UIControlStateNormal];
    [self.cheekbonesImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label = nil;
    label = [[UILabel alloc] initWithFrame:self.cheekbonesImageButton.frame];
    label.backgroundColor = [UIColor clearColor];
    label.text = NSLocalizedString(@"Cheekbones", nil);;
    label.font = [UIFont boldSystemFontOfSize: self.cheekbonesImageButton.frame.size.height];
    label.textColor = [UIColor redColor];
    label.shadowColor = [UIColor lightGrayColor];
    label.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label sizeToFit];
    label.center = self.cheekbonesImageButton.center;
    [self.view addSubview:label];
    [label release];
    label = nil;
    
    // 2. High / Defined Cheekbones
    
    [self.highOrDefinedImageButton  setImage:nil forState:UIControlStateNormal];
    [self.highOrDefinedImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label2 = nil;
    label2 = [[UILabel alloc] initWithFrame:self.highOrDefinedImageButton.frame];
    label2.backgroundColor = [UIColor clearColor];
    label2.text = NSLocalizedString(@"High / Defined Cheekbones", nil);
    label2.font = [UIFont boldSystemFontOfSize: self.highOrDefinedImageButton.frame.size.height];
    label2.textColor = [UIColor redColor];
    label2.shadowColor = [UIColor lightGrayColor];
    label2.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label2 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label2.center = self.highOrDefinedImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label2.center = self.highOrDefinedImageButton.center;
        }
        else
        {
            label2.center = CGPointMake(self.highOrDefinedImageButton.center.x, self.highOrDefinedImageButton.center.y);
        }
        
    }
    //label2.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:label2];
    [label2 release];
    label2 = nil;
    
    // 3.   Average Cheekbones    
    [self.averageImageButton setImage:nil    forState:UIControlStateNormal];
    [self.averageImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label3 = nil;
    label3 = [[UILabel alloc] initWithFrame:self.averageImageButton.frame];
    label3.backgroundColor = [UIColor clearColor];
    label3.text = NSLocalizedString(@"Average Cheekbones", nil);;
    label3.font = [UIFont boldSystemFontOfSize: self.averageImageButton.frame.size.height];
    label3.textColor = [UIColor redColor];
    label3.shadowColor = [UIColor lightGrayColor];
    label3.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label3 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label3.center = self.averageImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label3.center = self.averageImageButton.center;
        }
        else
        {
            label3.center = CGPointMake(self.averageImageButton.center.x, self.averageImageButton.center.y);
        }
        
    }
    
    [self.view addSubview:label3];
    [label3 release];
    label3 = nil;
    
    // 5.  Soft Cheekbones
    [self.softImageButton setImage:nil    forState:UIControlStateNormal];
    [self.softImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label5 = nil;
    label5 = [[UILabel alloc] initWithFrame:self.softImageButton.frame];
    label5.backgroundColor = [UIColor clearColor];
    label5.text = NSLocalizedString(@"Soft Cheekbones", nil);;
    label5.font = [UIFont boldSystemFontOfSize: self.softImageButton.frame.size.height];
    label5.textColor = [UIColor redColor];
    label5.shadowColor = [UIColor lightGrayColor];
    label5.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label5 sizeToFit];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        label5.center = self.softImageButton.center;
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            label5.center = self.softImageButton.center;
        }
        else
        {
            label5.center = CGPointMake(self.softImageButton.center.x, self.softImageButton.center.y);
        }
    }
    
    [self.view addSubview:label5];
    [label5 release];
    label5 = nil;
    
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    UILabel *label4 = [[UILabel alloc] initWithFrame:self.backImageButton.frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    label4.center = self.backImageButton.center;
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}

@end
