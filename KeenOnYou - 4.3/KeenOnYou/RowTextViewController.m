//
//  RowTextViewController.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/22/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "RowTextViewController.h"

@implementation RowTextViewController

@synthesize textView;


@synthesize namedFeature;

@synthesize namedFeatureString, featureString;

- (id)initWithNibName:(NSString *)nibNameOrNil 
               bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController.navigationBar setHidden:YES];
    self.textView.text = self.featureString;
    self.namedFeature.text = self.namedFeatureString;
    
    //DLog(@"self.textView.text = %@  self.namedFeature.text = %@", self.textView.text,self.namedFeature.text);
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        DLog(@" ERROR !!!");
    }
    else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
    {
        
        CGFloat scale = [[UIScreen mainScreen] scale];
        if (scale > 1.0)
        {
            if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                
            }
            else
            {
                
            }
        }
        else
        {
            
        }
        
    }
    else
    {
        DLog(@"WHAT THE MATTER WITH USER INTERFACE IDIOM?");
    }
    
    //  Localization of images on view
    NSString *currentLanguage = [[[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"] objectAtIndex:0];
    if (![currentLanguage isEqualToString:@"en"])
    {
        if ([currentLanguage isEqualToString:@"ja"] || [currentLanguage isEqualToString:@"zh-Hans"])
        {
            [self localizingThroughAddingLabels];
        }
    }


}

- (void)viewDidUnload
{
    [self setTextView:nil];
    [self setNamedFeature:nil];
    [self setBackImageButton:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)dealloc {
    
    [namedFeature release];
    [featureString release];
    [textView release];
    [namedFeature release];
    
    [_backImageButton release];
    [super dealloc];
}

#pragma mark  work with actions

- (IBAction)backButtonDidTap:(id)sender
{
    [Sound soundEffect:1];
    [self.navigationController popViewControllerAnimated:YES];

}

#pragma mark
#pragma mark customizing localiziable labels above images ...


- (void) localizingThroughAddingLabels
{
    
    CGRect frame;
    
    // 4. Back
    
    [self.backImageButton setImage:nil forState:UIControlStateNormal];
    [self.backImageButton setTitle:@" " forState:UIControlStateNormal];
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        frame = CGRectMake(91, 946, 123, 47);
    }
    else
    {
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            frame = CGRectMake(60, 507, 82, 31);
        }
        else
        {
            frame = CGRectMake(60, 417, 82, 31);
        }
    }
    
    
    
    UILabel *label4 = [[UILabel alloc] initWithFrame:frame];
    label4.backgroundColor = [UIColor clearColor];
    label4.text = NSLocalizedString(@"Back", nil);;
    label4.font = [UIFont boldSystemFontOfSize: self.backImageButton.frame.size.height];
    label4.textColor = [UIColor redColor];
    label4.shadowColor = [UIColor lightGrayColor];
    label4.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [label4 sizeToFit];
    //
    [self.view addSubview:label4];
    [label4 release];
    label4 = nil;
    
}



@end
