//
//  NoseUndersideViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface NoseUndersideViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *angledUpButton;
@property (retain, nonatomic) IBOutlet UIButton *straightButton;
@property (retain, nonatomic) IBOutlet UIButton *angledDownButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *noseUndersideImageButton;
@property (retain, nonatomic) IBOutlet UIButton *angledUpImageButton;
@property (retain, nonatomic) IBOutlet UIButton *straightImageButton;
@property (retain, nonatomic) IBOutlet UIButton *angledDownImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)angledUpButtonDidTap:(id)sender;
- (IBAction)straightButtonDidTap:(id)sender;
- (IBAction)angledDownButtonDidTap:(id)sender;

- (IBAction)backButtonDidTap:(id)sender;


@end
