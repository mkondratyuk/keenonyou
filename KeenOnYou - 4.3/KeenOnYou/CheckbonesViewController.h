//
//  CheckbonesViewController.h
//  KeenOnYou
//
//  Created by Kondratyuk on 1/18/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@class ReadFace;

@interface CheckbonesViewController : UIViewController // 

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *highOrDefinedButton;
@property (retain, nonatomic) IBOutlet UIButton *averageButton;
@property (retain, nonatomic) IBOutlet UIButton *softButton;

//  for localizing image/text
@property (retain, nonatomic) IBOutlet UIButton *cheekbonesImageButton;
@property (retain, nonatomic) IBOutlet UIButton *highOrDefinedImageButton;
@property (retain, nonatomic) IBOutlet UIButton *averageImageButton;
@property (retain, nonatomic) IBOutlet UIButton *softImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)highOrDefineButtoDidTap:(id)sender;
- (IBAction)averageButtonDidTap:(id)sender;
- (IBAction)softButtonDidTap:(id)sender;


- (IBAction)backButtonDidTap:(id)sender;


@end
