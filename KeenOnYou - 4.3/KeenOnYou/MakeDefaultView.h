//
//  MakeDefaultView.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 2/6/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>


@interface MakeDefaultView : UIViewController
@property (retain, nonatomic) IBOutlet UIImageView *backImageView;
@property (retain, nonatomic) IBOutlet UIImageView *logoImageVie;
@property (retain, nonatomic) IBOutlet UILabel *label;



-(void)saveCurrentAvatarImageInFilePNG;

@end
