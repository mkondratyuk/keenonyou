//
//  ListSavedFaceCell.m
//  KeenOnYou
//
//  Created by  Kondratyuk on 1/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListSavedFaceCell.h"

@implementation ListSavedFaceCell
@synthesize avatarView;
@synthesize nameAvatarLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc {
    [avatarView release];
    [nameAvatarLabel release];
    [super dealloc];
}
@end
