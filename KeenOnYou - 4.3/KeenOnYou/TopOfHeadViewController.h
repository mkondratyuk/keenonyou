//
//  TopOfHeadViewController.h
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 1/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ReadFace;

@interface TopOfHeadViewController : UIViewController

@property (nonatomic, retain) ReadFace *currentReadFace;

@property (retain, nonatomic) IBOutlet UIButton *levelHeadButton;
@property (retain, nonatomic) IBOutlet UIButton *highFrontButton;
@property (retain, nonatomic) IBOutlet UIButton *highBackButton;
// localization group
@property (retain, nonatomic) IBOutlet UIButton *topOfHeadImageButton;
@property (retain, nonatomic) IBOutlet UIButton *highFrontImageButton;
@property (retain, nonatomic) IBOutlet UIButton *levelHeadImageButton;
@property (retain, nonatomic) IBOutlet UIButton *highBackImageButton;
@property (retain, nonatomic) IBOutlet UIButton *backImageButton;

- (IBAction)levelHeadDidTap:(id)sender;
- (IBAction)highFrontDidTap:(id)sender;
- (IBAction)highBackDidTap:(id)sender;

- (IBAction)backButtonDidTap:(id)sender;

@end
