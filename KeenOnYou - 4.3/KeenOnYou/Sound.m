//
//  Sound.m
//  KeenOnYou
//
//  Created by Mykola Kondratyuk on 2/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Sound.h"

@interface Sound()




@end


@implementation Sound

+ (void) soundEffect:(int)soundNumber {
    NSString *effect = nil;
    NSString *type = nil;
    if (soundNumber == 0) {
        effect = @"Delete_Face_Completed";
        type = @"aif";//type = @"mp3";
    }
    else if (soundNumber == 1) {
        effect = @"Button_Click";
        type = @"aif";//type = @"mp3";
    }
    else if (soundNumber == 2) {
        effect = @"Face_Created";
        type = @"aif";//type = @"mp3";
    }
    else
    {
        DLog(@"sound effect did not true !!!");
    }
    
    NSString *value = [[NSUserDefaults standardUserDefaults] stringForKey:@"sound"];
    if ([value compare:@"ON"] == NSOrderedSame)
    {
        
        SystemSoundID soundID;
        
        NSString *path = [[NSBundle mainBundle] pathForResource:effect ofType:type];
        NSURL *url = [NSURL fileURLWithPath:path];
        
        AudioServicesCreateSystemSoundID ((CFURLRef)url, &soundID);
        
        AudioServicesPlaySystemSound(soundID);
        
    }
}

- (void)dealloc
{
    [super dealloc];
}



#pragma mark C function


void SoundFinished (SystemSoundID snd, void* context) 
{
    AudioServicesRemoveSystemSoundCompletion(snd);
    AudioServicesDisposeSystemSoundID(snd);
}


#pragma mark

- (void) playSystemSoundFromMainBundleWithFileName: (NSString *) name
                               andExtension: (NSString *) ext
{
    
    NSURL* sndurl = [[NSBundle mainBundle] URLForResource:name 
                                            withExtension:ext];
    SystemSoundID snd;
    AudioServicesCreateSystemSoundID((CFURLRef)sndurl, &snd);   
    AudioServicesAddSystemSoundCompletion(snd, NULL, NULL, &SoundFinished, NULL);
    AudioServicesPlaySystemSound(snd);
}

@end